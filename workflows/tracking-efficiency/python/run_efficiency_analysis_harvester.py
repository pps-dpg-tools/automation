import sys
from ecalautoctrl import HTCHandlerByRun
        
if __name__ == '__main__':
    handler = HTCHandlerByRun(task='tracking-efficiency-analysis-harvester',
                              prev_input='tracking-efficiency-analysis-worker',
                              deps_tasks=['tracking-efficiency-analysis-worker'])

    ret = handler()
    
    sys.exit(ret)
