# Introduction

This guide is meant to collect all the information related to the framework that 
provides the automation of the CMS PPS calibration and monitoring workflows.

The framework was designed and developed during the LHC LS2 and is currently in its
commissioning phase. The original developer of the framework for ECAL is [Simone Pigazzini](mailto:simone.pigazzini@cern.ch). All credits to him!

The system was designed following these principles:

- Rely as much as possible on market software, minimizing the need of ad-hoc code.
- Integrate the existing calibration workflows limiting the amount of code re-writing.
- Provide simple monitoring tools (here monitoring refers to the monitoring of the
  automation, not of the PPS detectors).
- Support both prompt-reco and re-reco calibration campaigns (ideally without the need to
  adjust the workflow too much when moving from one to the other).

As PPS, we started integrating our calibration workflows in 2021. Over time we diverged and re-merged into the original ECAL schema, and are now trying to remain loyal to it.
If a PPS-related feature is needed, Simone is generally willing to have it integrated in the automation framework, thus we can develop it and have it integrated in the shared repository.
  
## Calibration goals

The main goal of the automation system is to provide performance monitoring (and possibly calibration) as close in time as possible to the prompt reconstruction.

For PPS, this currently reflects in:

- **Pixel detector:** efficiency measurements (plane, radiation, multiRP proton).
- **Timing detector:** timing resolution measurement and calibration.

## Documentation structure

The documentation is divided in 4 parts, characterized by different topics and technical depths:

- ![](https://progress-bar.dev/0?title=Completion) **For users:** users are foreseen to be interested in monitoring the execution as [shifters](shifterguide.md) or [inspecting results](accessing-results.md). Here they can find the instructions needed for basic actions.
- ![](https://progress-bar.dev/100?title=Completion) **For calibration developers:** these instructions are for experts that implement calibrations and test them. They have higher access rights and need to know how to develop and test their workflows.
- ![](https://progress-bar.dev/100?title=Completion) **For maintainers:** this section contains reference information for people who take care of the infrastructure necessary for the automation framework to exist (InfluxDB, Jenkins, Grafana).
- ![](https://progress-bar.dev/20?title=Completion) **Workflows:** each workflow has its own documentation, where the calibration developer is supposed to explain how the calibration works, and provide instructions to test or run it.

## Useful links

- [**PPS automation repository**](https://gitlab.cern.ch/pps-dpg-tools/automation)
- [**automation-control repository (ecalautoctrl python package)**](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control)
- [**Grafana instance**](https://ecal-automation-monit.web.cern.ch)
- [**Jenkins instance**](https://dpg-ecal-calib.web.cern.ch/)
- [**CTPPS CMSSW**](https://github.com/CTPPS/cmssw)
  - The automation-related branches are called *rolling-calib-&lt;calibration_name&gt;-&lt;CMSSW_version&gt;*