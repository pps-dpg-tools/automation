import sys
from ecalautoctrl import HTCHandlerByRun
        
if __name__ == '__main__':
    handler = HTCHandlerByRun(task='plotters',
                              prev_input='re-alcareco',
                              deps_tasks=['re-alcareco'])

    ret = handler()
    
    sys.exit(ret)
