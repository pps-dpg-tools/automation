# Run control
This is a very simple workflow that executes a single Jenkinsfile.

The Jenkinsfile encodes the steps performed by the main run controller:
   - Periodically check the run measurement for completed runs, and mark them as such.
   - sync the automation database with the run registry by querying OMS, if running in prompt or repro modes.