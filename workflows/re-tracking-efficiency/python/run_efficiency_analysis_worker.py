import sys
from ecalautoctrl import HTCHandlerByRun
        
if __name__ == '__main__':
    handler = HTCHandlerByRun(task='re-tracking-efficiency-analysis-worker',
                              prev_input='tracking-efficiency-alcareco',
                              dsetname='/AlCaPPSPrompt/*/ALCARECO')

    ret = handler()    
    sys.exit(ret)
