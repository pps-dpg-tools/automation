import sys

from ecalautoctrl import HTCHandlerByRunDBS


if __name__ == '__main__':
    handler = HTCHandlerByRunDBS(task='timing-resolution-worker',
                                 dsetname='/AlCaPPSPrompt/*/ALCARECO')

    ret = handler()
    sys.exit(ret)
