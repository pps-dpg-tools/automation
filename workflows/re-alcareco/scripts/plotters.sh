#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/p/ppsgit/

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"


# Get the run number from the script arguments
run_number=$(echo "$TASK" | grep -oP "(?<=run_number:)\d+")

# Make following commands independent from the json file used
mv *.json jsonFilter.json

# Run the job
cmsRun $WDIR/src/RecoPPS/RPixEfficiencyTools/test/runPlotters.py inputFiles=$INFILE globalTag=$GT jsonFileName=jsonFilter.json
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    mkdir -p $EOSDIR
    OFILE_LHCINFO=PPS_ALCARECO_plots_lhcInfo_"$run_number".root
    OFILE_TRACK=PPS_ALCARECO_plots_trackDistribution_"$run_number".root
    OFILE_PROTON=PPS_ALCARECO_plots_protonReconstruction_"$run_number".root
    mv PPS_ALCARECO_plots_lhcInfo.root $EOSDIR/$OFILE_LHCINFO
    mv PPS_ALCARECO_plots_trackDistribution.root $EOSDIR/$OFILE_TRACK
    mv PPS_ALCARECO_plots_protonReconstruction.root $EOSDIR/$OFILE_PROTON
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE_LHCINFO},${EOSDIR}/${OFILE_TRACK},${EOSDIR}/${OFILE_PROTON}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
