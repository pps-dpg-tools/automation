import sys
from ecalautoctrl import HTCHandlerByRunDBS
        
if __name__ == '__main__':
    handler = HTCHandlerByRunDBS(task='re-alcareco',
                                 dsetname='/ZeroBias/*/AOD')

    ret = handler()    
    sys.exit(ret)
