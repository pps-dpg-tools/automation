#!/usr/bin/env groovy

def call(String level = 'INFO', String msgtext = "$JOB_BASE_NAME: failed job execution.", String notify_url=env.notify_url) {
    // Define statuses color map
    def levels = [INFO : '#4287f5',
                  SUCCESS : '#1be800',
                  WARNING : '#ff9100',
                  ERROR : '#cf0000',
                  CRITICAL : '#000000']

    def color = levels[level]
    
    httpRequest(httpMode: 'POST', 
                contentType: 'APPLICATION_JSON',
                requestBody: "{\"attachments\": [{ \"color\" : \"${color}\", \"text\" : \"${msgtext}\", \"pretext\" : \"$BUILD_URL\" }]}",
                url: notify_url)
}
