import sys
from ecalautoctrl import HTCHandlerByRun
        
if __name__ == '__main__':
    handler = HTCHandlerByRun(task='re-tracking-efficiency-reference-harvester',
                              prev_input='re-tracking-efficiency-reference-worker',
                              deps_tasks=['re-tracking-efficiency-reference-worker'])

    ret = handler()
    
    sys.exit(ret)
