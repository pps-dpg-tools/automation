[pps-automation-git]: https://gitlab.cern.ch/pps-dpg-tools/automation

# Running environments

The [PPS automation repository][pps-automation-git] is used to generate docker images (via GitLab CI), which are then used by Jenkins to execute the tasks of each workflow (see the [AutomationApptainerStep](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/shared-lib/vars/automationApptainerStep.groovy)). Everything concerning the docker images is defined in the [docker directory](https://gitlab.cern.ch/pps-dpg-tools/automation/-/tree/master/docker) of the repository.

GitLab builds multiple images as defined [here](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docker/.build-img-ci.yml):

- *dev* image: built from the master branch, it's the environment used for testing new workflows.
- *prompt* image: built from the master branch when a `*-prompt` tag is created. It's used by the Jenkins' items marked as 'prompt', which run online while taking data.
- *repro* image: built from the master branch when a `*-repro` tag is created. It's used by the Jenkins' items marked as 'repro', which run when an official reprocessing campaign is launched.

Instructions to access the docker images are provided in other sections of the documentation (when it's needed).

Different running environments can use different global variables, which are defined [here](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/shared-lib/src/ppsctrl/vars/GlobalVars.groovy).

