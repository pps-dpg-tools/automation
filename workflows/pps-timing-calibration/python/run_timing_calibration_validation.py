import os
import subprocess
import sys

from dataclasses import dataclass
from typing import Any, Dict, List, Set, Tuple

from ecalautoctrl import AutoCtrlScriptBase, JobCtrl


class TimingCalibrationValidation(AutoCtrlScriptBase):
    """
    Checks:
    1. If any of the channels have both the time offset and time precision set to 0.0.
    2. If an odd (even) plane has a double peak detected, whether its even (odd) counterpart has it spotted or not.
    3. If all of the channels have their fit parameters set to 0.0.

    Results:
    1. The *timing-calibration-is-double-peak-correction-needed* field will be set to True,
       and *double_peak_tasks* will be marked for reprocessing. Otherwise, that field will be set to False.
    2. If the other plane is not among the double peak planes,
       *task* will be marked as 'failed', and *tasks_to_skip* will be marked as 'skipped'.
    3. *task* will be marked as 'failed', and *tasks_to_skip* will be marked as 'skipped'.
       Otherwise, *task* will be marked as 'done'.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    :param double_peak_tasks: list of double peak tasks.
    :param tasks_to_skip: list of tasks which will be skipped if validation fails.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: List[str],
                 double_peak_tasks: List[str],
                 tasks_to_skip: List[str],
                 **kwargs):
        super().__init__(task=task, **kwargs)

        self.prev_input: str = prev_input
        self.deps_tasks: List[str] = deps_tasks
        self.double_peak_tasks: List[str] = double_peak_tasks
        self.tasks_to_skip: List[str] = tasks_to_skip

        self.subparsers = self.parser.add_subparsers(dest='subcommand',
                                                     description='Select command to execute')
        self.check_parser = self.subparsers.add_parser('check',
                                                       help='Check results of the timing calibration workflow')
        self.check_parser.set_defaults(cmd=self.check)

    @dataclass
    class Payload:
        sector: int
        station: int
        plane: int
        channel: int
        fit_parameters: List[float]
        time_offset: float
        time_precision: float

    @staticmethod
    def parse_payload(payload: str) -> Payload:
        detector_params, _, fit_parameters_and_resolution_parameters = payload.partition('[')
        sector, station, plane, channel = map(int, detector_params.split())
        fit_parameters_comma_separated, _, resolution_parameters = fit_parameters_and_resolution_parameters.partition(']')
        fit_parameters: List[float] = list(map(float, fit_parameters_comma_separated.split(', ')))
        time_offset, time_precision = map(float, resolution_parameters.split())
        return TimingCalibrationValidation.Payload(sector=sector,
                                                   station=station,
                                                   plane=plane,
                                                   channel=channel,
                                                   fit_parameters=fit_parameters,
                                                   time_offset=time_offset,
                                                   time_precision=time_precision)

    def check(self):
        """
        Check results of the timing calibration workflow.
        If all channels have zero params, mark this task as 'failed'.
        """
        PAYLOAD_HEADER: str = 'DB SAMPIC CHANNEL CELL PARAMETERS TIME_OFFSET'
        SECTORS, STATIONS, PLANES, CHANNELS = 2, 2, 4, 12
        NUM_OF_PAYLOAD_ENTRIES: int = SECTORS * STATIONS * PLANES * CHANNELS
        ZERO_PARAMS: List[float] = [0.0, 0.0, 0.0, 0.0]
        DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME: str = 'timing-calibration-is-double-peak-correction-needed'

        active_tasks: Set[str] = {active_task for task in self.rctrl.getWorkflows().values() for active_task in task['active']}
        active_tasks_to_skip: Set[str] = set(self.tasks_to_skip).intersection(active_tasks)
        runs: List[Dict[str, Any]] = self.rctrl.getRuns(status={dep_task: 'done' for dep_task in self.deps_tasks}, active=True)
        for run in runs:
            if run[self.task] != 'done':
                run_number: str = run['run_number']
                self.rctrl.updateStatus(run=run_number, status={self.task: 'processing'})
                jctrl = JobCtrl(task=self.prev_input,
                                campaign=self.campaign,
                                tags={'run_number': run_number, 'fill': run['fill']},
                                dbname=self.opts.dbname)
                sqlite_path: str
                sqlite_path, _ = jctrl.getJob(jid=0, last=True)[0]['output'].split(',')  # The 2nd part after comma is the .root file

                sqlite_output = subprocess.run([
                    'cmsRun',
                    f'{os.environ["CMSSW_BASE"]}/src/CalibPPS/ESProducers/test/ppsTimingCalibrationAnalyzer_cfg.py',
                    f'runNumber={run_number}',
                    f'sqlitePath={sqlite_path}',
                    'tag=DiamondTimingCalibration'
                ], stdout=subprocess.PIPE)
                sqlite_parsed_output: List[str] = sqlite_output.stdout.decode().split('\n')
                first_entry_index: int = sqlite_parsed_output.index(PAYLOAD_HEADER) + 1

                is_double_peak_correction_needed: bool = False
                double_peak_planes: Set[Tuple[int, int, int]] = set()
                zero_fit_parameters: int = 0
                for i in range(first_entry_index, first_entry_index + NUM_OF_PAYLOAD_ENTRIES):
                    parsed_payload: TimingCalibrationValidation.Payload = TimingCalibrationValidation.parse_payload(sqlite_parsed_output[i])
                    if parsed_payload.time_offset == 0.0 and parsed_payload.time_precision == 0.0:
                        is_double_peak_correction_needed = True
                        double_peak_plane: Tuple[int, int, int] = (parsed_payload.sector, parsed_payload.station, parsed_payload.plane)
                        if double_peak_plane not in double_peak_planes:
                            self.log.info(f'Double peak in plane {double_peak_plane} for Run {run_number}.')
                            double_peak_planes.add(double_peak_plane)
                    elif parsed_payload.fit_parameters == ZERO_PARAMS:
                        zero_fit_parameters += 1

                for double_peak_plane in double_peak_planes:
                    plane_number: int = double_peak_plane[2]
                    if plane_number == 1 or plane_number == 3:
                        counterpart_plane: Tuple[int, int, int] = (double_peak_plane[0], double_peak_plane[1], plane_number - 1)
                    else:
                        counterpart_plane: Tuple[int, int, int] = (double_peak_plane[0], double_peak_plane[1], plane_number + 1)
                    if counterpart_plane not in double_peak_planes:
                        self.rctrl.updateStatus(run=run_number, status={self.task: 'failed'} | {task_to_skip: 'skipped' for task_to_skip in active_tasks_to_skip})
                        self.log.info(f'Double peak not detected in plane {counterpart_plane} but spotted in plane {double_peak_plane}. Tasks {active_tasks_to_skip} skipped.)')
                        break

                else:
                    if self.rctrl.getStatus(run=run_number)[0].get(DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME) is None:
                        if is_double_peak_correction_needed:
                            self.rctrl.updateStatus(run=run_number,
                                                    status={DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME: True} | {double_peak_task: 'reprocess' for double_peak_task in self.double_peak_tasks})
                        else:
                            self.rctrl.updateStatus(run=run_number,
                                                    status={DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME: False, self.task: 'done'})

                    elif zero_fit_parameters == NUM_OF_PAYLOAD_ENTRIES:
                        self.rctrl.updateStatus(run=run_number, status={self.task: 'failed'} | {task_to_skip: 'skipped' for task_to_skip in active_tasks_to_skip})
                        self.log.info(f'Timing calibration validation failed for Run {run_number}. Tasks {active_tasks_to_skip} skipped.')

                    else:
                        self.rctrl.updateStatus(run=run_number, status={self.task: 'done'})


if __name__ == '__main__':
    validator = TimingCalibrationValidation(task='timing-calibration-validation',
                                            prev_input='timing-calibration-harvester',
                                            deps_tasks=['timing-calibration-harvester'],
                                            double_peak_tasks=['timing-calibration-worker',
                                                               'timing-calibration-harvester'],
                                            tasks_to_skip=['timing-resolution-worker',
                                                                'timing-resolution-harvester',
                                                                'timing-resolution-iteration'])

    ret = validator()
    sys.exit(ret)
