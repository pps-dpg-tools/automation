# Welcome to the PPS Automation Framework

Please find the most up-to-date documentation [here](https://pps-automation.docs.cern.ch/).

Credits to [S. Pigazzini](mailto:simone.pigazzini@cern.ch) for the original project, on which the PPS one is based.