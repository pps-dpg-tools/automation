import sys

from ecalautoctrl import HTCHandlerByRunDBS


if __name__ == '__main__':
    handler = HTCHandlerByRunDBS(task='timing-calibration-worker',
                                 dsetname='/AlCaPPSPrompt/*/ALCARECO')

    ret = handler()
    sys.exit(ret)
