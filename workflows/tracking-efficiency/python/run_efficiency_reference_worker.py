import sys
import subprocess
from os import path
from typing import Optional, List, Dict
from itertools import islice


from ecalautoctrl import HTCHandler,process_by_run,QueryDBS,JobCtrl
from ecalautoctrl.TaskHandlers import get_files_prev_task

@process_by_run
class ReferenceAnalysisHandlerDBS(HTCHandler):
    """Works as an HTCHandler that always gets the same run,
    and gets an input from a previous workflow.
    """
    def __init__(self,
                task: str,
                dsetname: str,
                ref_runs: List[str],
                prev_input: str,
                deps_tasks: Optional[List[str]]=None,
                **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname
        self.ref_runs = ref_runs
        self.prev_input = prev_input

    def get_files_dbs(self, process_runs: List[Dict],reference_runs: List[str]) -> List[str]:
        """
        Retrieve file names for given runs from DBS.
        This function is added by :deco:`~ecalautoctrl.dbs_data_source`.

        :param process_runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
        This are the runs for which the efficiency computation is made.
        :param reference_runs: list of runs to be used as reference dataset.
        """
        flist = []
        try:
            if self.dsetname:
                for run in process_runs:
                    _,pd,sd,tier = self.dsetname.split('/')

                    # add era to dataset name, validating it in the process
                    # sd = '*' + run["era"] + sd
                    # skipping this, because the era of the run being processed might not match
                    # the era of the reference data

                    fullname = f'/{pd}/{sd}/{tier}'
                    fromt0 = self.opts.t0 if 't0' in self.opts else False
                    dataQuery = QueryDBS(dataset=fullname)
                    for ref_run in reference_runs:
                        flist.extend(dataQuery.getRunFiles(run=int(ref_run), fromt0=fromt0))
            return flist
        except AttributeError as ae:
            raise AttributeError('Please define self.dsetname in your class when using the dbs_data_source decorator')

    def submit(self):
        """
        Submit runs in status task: new.
        Resubmit runs in status task: reprocess.
        Mainly from HTCHandler.submit (need to edit the condor arguments)
        """

        for group in self.groups():
            # get efficiency input file
            prev_output = get_files_prev_task(self,group)
            effInput = prev_output[0]

            # master run
            run = group[-1]
            # Get data from DBS for reference dataset
            fdict = self.get_files_dbs(process_runs=group,reference_runs=self.ref_runs)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr = (run[self.task] == 'reprocess')
                if jctrl.taskExist() and run[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        allow_repr = False
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    # split the file list into groups accordingly to nfiles,
                    # apexes are added around join(f) to ensure condor will later
                    # interpret the comma separated list as a single item
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    self.opts.nfiles = len(fdict) if self.opts.nfiles==-1 else self.opts.nfiles
                    it = iter(fdict)
                    flist = [",".join([f for f in ff]) for ff in iter(lambda: tuple(islice(it, self.opts.nfiles)), ())]
                    with open('args.txt', 'w') as ff:
                        ff.write('\n'.join([f for f in flist]))
                    # get the global tag from the influxdb.
                    # if not set leave GT empty (rely on auto cond).
                    gt = run['globaltag'] if 'globaltag' in run else '\'\''
                    self.log.info(f'GlobalTag: {gt}')
                    ret = subprocess.run(['condor_submit',
                                        f'{self.opts.template}',
                                        '-spool',
                                        '-queue', '1 fname from args.txt',
                                        '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {effInput}"',
                                        '-append', 'max_retries = 0'],
                                        capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - log file
                        jctrl.createTask(jids=list(range(len(flist))),
                                        recreate=allow_repr,
                                        fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                'inputs': f,
                                                'log': f'{logurl}/{self.task}-{cluster}-{i}.log'} for i,f in enumerate(flist)])
                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        """

        runs = self.rctrl.getRuns(status={self.task: 'processing'})


        for run_dict in runs:
            # get efficiency input file
            prev_output = get_files_prev_task(self,runs)
            effInput = prev_output[0]

            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # actually running in condor.
            for jid in jctrl.getRunning():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)
            # resubmit failed
            if not jctrl.taskExist() or jctrl.getFailed() == []:
                self.log.info(f'no failures found for run {run}')
            else:
                self.log.info(f'found failed job for run {run}')
                # Building the resubmission arguments
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                # move to AAA if file can't be read from T0 eos.
                flist = []
                for i in jctrl.getFailed():
                    inputs = jctrl.getJob(i, last=True)[0]['inputs']
                    if '/eos/cms/tier0' in inputs and not all([path.isfile(p) for p in inputs.split(',')]):
                        inputs = inputs.replace('file:/eos/cms/tier0', 'root://cms-xrd-global.cern.ch/')
                    flist.append(i+', '+inputs)
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                with open('args.txt', 'w') as ff:
                    ff.write('\n'.join(flist))
                # get the global tag from the influxdb.
                # if not set leave GT empty (rely on auto cond).
                gt = run_dict['globaltag'] if 'globaltag' in run_dict else '\'\''
                self.log.info(f'GlobalTag: {gt}')
                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'jobid, fname from args.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(jobid) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {effInput}"',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"',
                                      '-append', 'max_retries = 0'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    # set the job status to idle
                    for i in jctrl.getFailed():
                        jctrl.idle(jid=i)
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0



if __name__ == '__main__':
    handler = ReferenceAnalysisHandlerDBS(task='tracking-efficiency-reference-worker',
                                       dsetname='/AlCaPPSPrompt/*/ALCARECO',
                                       ref_runs=['366504'],
                                       prev_input='tracking-efficiency-analysis-harvester',
                                       deps_tasks=['tracking-efficiency-analysis-harvester'])

    ret = handler()

    sys.exit(ret)
