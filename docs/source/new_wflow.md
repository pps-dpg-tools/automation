[pps-automation-git]: https://gitlab.cern.ch/pps-dpg-tools/automation
[ecalautoctrl-git]: https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/
[ecalautoctrl-docs]: https://cmsecaldocs.web.cern.ch/ecalautoctrl/ 
[rctrl]: https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.RunCtrl
[jctrl]: https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#jobctrl

# Creating new workflows

## Workflow code structure

We try to keep the workflow directory structures coherent, in order to maintain the code reasonably readable. Aesthetic scopes aside, the structure is completely free and flexible, with the exception of one item: each workflow directory must contain a **Jenkinsfile** at the top level, which is used to steer the Jenkins pipeline.

Usually the workflow directory is composed as follows:

- **python directory:** contains the python files that define the TaskHandlers for each task.
- **templates directory:** contains the template .sub files for HTCondor, to be edited and submitted by the TaskHandler.
- **scripts directory:** contains the scripts to be executed by HTCondor when the job is run.

The current directories don't cover some aspects, e.g. CMSSW config files, possible inputs/configurations. If that happened to be needed, I suggest discussing this decision with the [pps-automation repository][pps-automation-git] maintainers, in order to find a solution that can be made official.

??? Note 
    Drawing a line between what is calibration code and what is configuration is at times not straightforward.
    For instance, regarding tasks that run CMSSW we for sure won't include a separate version of CMSSW code into the automation repository, but there are workflows for which only a dedicated `_cfi.py` is needed, and therefore it seems impractical to just have an extra repository to host a single file. 
    In such a case the single python file is considered as configuration and is hosted in the automation repo. 
    In other cases where new plugins are added to existing CMSSW packages or new packages are added altogether it is natural to have a separate repository for that.


At the moment, the best example is provided by the [tracking efficiency workflow](workflows/tracking-efficiency/README.md) (namely by the *tracking-efficiency-analysis-worker* and *tracking-efficiency-analysis-harvester* tasks). A true template task is under development. 

## Add a workflow to the git repo

Each workflow has its own directory in the **master** branch of the [automation repository][pps-automation-git]. In order for a new workflow to be implemented, the calibration developer can setup its workspace as explained below:

- The user asks for 'developer' rights on the [automation repository][pps-automation-git] to the maintainers. This will be needed in order to push new branches to the repo and test the automated execution. In the context of the same request, the user should also ask for DB credentials.
- The user clones the project to its development area.
- The user creates a new branch, based on the master, called **dev-&lt;calibration name&gt;**.
- The user [develops the workflow](new_wflow.md#workflow-development-process) (see below).

## Workflow development process

Below are listed the milestones to develop a new workflow. Ideally, they should be carried out in the listed order, as following steps rely on the previous ones. 

!!! Note
    The *further requirements* allow for better profiting of the features already implemented in the automation-control repository. They are by no means compulsory and there are many other ways of achieving the same results, however it's worth keeping them in mind.

1. **Local development:** get your code running locally. As a first requirement, one should have a version of the calibration code that runs locally (i.e. with commands executed by the user on lxplus). E.g., implement your CMSSW calibration code and run each `cmsRun` command locally, checking that it produces outputs starting from the expected inputs.<br>
*Further requirements:* 
    - CMSSW python configs should allow for some command line inputs to `cmsRun` (use FWCore.ParameterSet.VarParsing)
        - *inputFiles:* comma-separated list of input files. Local files will not be prepended with the 'file:' tag. This should be done in the CMSSW python config.
        - *globalTag:* name of the globaltag to be used 
2. **Distributed running:** check that your code runs when submitted on HTCondor (or any other distributed computing system that might be used/implemented).<br>
*Further requirements (HTCondor):* 
    - A template submit file should be generated, following the example of the [tracking-efficiency workflow](https://gitlab.cern.ch/pps-dpg-tools/automation/-/tree/master/workflows/tracking-efficiency). 
        - Some parameters will be automatically replaced when jobs are submitted (search for the `--template` option of the `HTCHandler` class).
3. **Task implementation:** implement and test the new workflow.
    - Create the workflow directory in the local branch of the [automation repository][pps-automation-git] following the [instruction above](#add-a-workflow-to-the-git-repo).
    - Add to the docker image [build script](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docker/build.sh) your dependencies (CMSSW branches, python packages, etc.)
        - **Do not remove any already existing dependency.** In case this is needed, talk to a maintainer as this action might break other workflows.
    - Push your *dev-&lt;calibration name&gt;* branch to the [automation repository][pps-automation-git].
        - This will trigger an automatic GitLab CI pipeline that will build your dedicated docker image. Verify that this happens without errors by checking the [pipeline status](https://gitlab.cern.ch/pps-dpg-tools/automation/-/pipelines).
        - After it is built, the image will be automatically unpacked on *cvmfs*. Wait for it to be available (you can check with [these commands](instructions.md#checking-the-docker-image)). The unpacked image will be named: <br>
        */cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/pps-dpg-tools/automation:dev-&lt;calibration name&gt;*<br>
        This should happen in about 20 minutes.
            - You will need to **re-build the image** every time you change something in your CMSSW implementation. In this case, trigger the re-build by adding a negligible change in the [build script](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docker/build.sh) (a space), committing it with an explanatory message, and pushing the change to the [automation repository][pps-automation-git].
    - Implement all the necessary code and tasks (see more details [below](#task-implementation)) for the workflow to run.
    - Write the Jenkinsfile that should be used by the automated execution.
    - Test the Jenkinsfile in your docker image by [executing it manually](instructions.md#manually-testing-a-workflow).
4. **Grafana (in parallel with 3.):** monitor the execution on [Grafana](https://ecal-automation-monit.web.cern.ch) during tests (ask a maintainer for access rights to modify the dashboard or have your own).
5. **Jenkins:** 
    - Open a merge request to the master branch and contact a maintainer. 
    - A maintainer will review and merge the new calibration in the master branch.
    - The maintainer will setup the Jenkins pipeline for the automated execution..
6. **Documentation:** this is the last, but definitely not the least item in the list. Developing the documentation while implementing the workflow is strongly advised. Please follow the [docs-development instructions](writing-docs.md).

### Task implementation

Each task is handled by a [TaskHandler class](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#taskhandlers). The [base handler class](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.HandlerBase) provides a skeleton for the actual implementation of a class that can handle all the task features and transitions. 

The [AutoCtrlScriptBase class](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.TaskHandlers.AutoCtrlScriptBase) on which all handlers are based provides the features needed to quickly turn a python class into an executable script. This feature provides a certain degree of flexibility: use the class from interactive python while developing and packaging all the functions into a predefined script structure to be used by the Jenkinsfile in production.

Pretty generic TaskHandlers are already provided by the automation-control repository. However, in case of special needs, some further documentation of how they work is provided below.

The handler controls the task status and execution by interfacing with the Influx database. A [RunCtrl][rctrl] instance is created as a member of the class (accessible as `self.rctrl`). The [RunCtrl][rctrl] provides the interface to the `run` status table in the database, where the information about the status of each task is stored for each CMS DAQ run.

The handler also needs an instance of the [JobCtrl][jctrl] in order to interact with the `job` table in the database. The `job` table contains the status of all jobs executed by the automation. The [JobCtrl][jctrl] init options are used to select the jobs belonging to a given task.
Contrary to the [RunCtrl][rctrl] instance the [JobCtrl][jctrl] one is not created as a class member by the base class but is instantiated on the fly within the handler methods. This is necessary since a different instance has to be created for each run number.

The handler class should implement the `submit`, `resubmit`, `check` methods. These define the 3 crucial status transitions a task goes through:

| class method | state transitions            |
|--------------|------------------------------|
| submit       | "new" -> "processing"        |
|              | "reprocess" -> "processing"  |
| resubmit     | "processing" -> "processing" |
| check        | "processing" -> "done"       |
|              | "processing" -> "failed"     |

The state here referes to the task, i.e. "failed" means that at least one job has been marked as permanently failed (after a number of retries that is specified as a command line option to the check command).

Failed jobs are supposed to be resubmitted by calling the `resubmit` method by the handler while permanent failures, once marked as such by the `check` method, requires manual intervention to be fixed. The manual resubmission will set the status of the task to `reprocess` to clearly mark in the task history that the task went through a manual resubmission.

The set of methods (`submit`, `resubmit` and `check`) is not strictly defined in the system. Each task can in principle define more or less. Nonetheless, the 3 methods provide flexibility for the majority of tasks and simplicity. As explained in the [Workflow and task concepts](definitions.md) section, if a task requires two jobs to be executed in parallel one should consider splitting the task into two separate ones. Keeping the task as atomic as possible should ensure the 3 methods above are enough to implement every task.
The typical task progression follows this scheme:

```mermaid
graph LR
  N[new] --> |<b><mark>submit</mark></b>| P[processing];
  P --> |<mark><b>resubmit</b></mark>| P;
  P --> C{<mark><b>check</b></mark>};
  C --> D[done];
  C --> F[failed];
  F --> |<i>manual reprocess</i>| R[reprocess];
  R --> P;
```

The `check` method provide by the [HandlerBase](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.HandlerBase.check) is general enough to fit all tasks implemented so far.
The `submit` and `resubmit` methods instead are generalized only for submission of simple [HTCondor jobs](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.HTCHandler).

Single job tasks require the implementation of a dedicated handler class following the standard structure explained below. Although fairly general, the HTCHandler class might not cover all the use cases in which parallel jobs are required, thus one should extend it or implement a new class.

The handler classes available in the `ecalautoctrl` package do not implement directly the `groups` and `files` methods. These two methods should provide, respectively, the grouping of runs to be processed together and the relevant input files for each group.
The implementation of the two methods are left to derived classes and decorators. Decorators offer a quick way to provide different classes with the same functionality, akin to multiple inheritance but with a higher level of flexibility.
A number of decorators are implemented in the `ecalautoctrl` package and provide standard grouping and file access like: [group_by_run](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.process_by_run), [group_by_fill](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.process_by_fill), [group_by_intlumi](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.process_by_intlumi), [prev_task_data_source](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.prev_task_data_source) and [dbs_data_source](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.dbs_data_source).
A set of handler specialized with the above decorators is also provided like: [HTCHandlerByFill](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.HTCHandlerByFill). Dedicated ones can be created on the fly in and stored in the workflow branch like:

```python
@prev_task_data_source # (1)
@process_by_fill(fill_complete=True) # (2)
class ZeeMonHandler(HandlerBase):
    """
    Execute all the steps to generate the Zee monitoring plots.
    Process fills that have been dumped (completed).

    :param task: workflow name.
    :param prev_input: name of the workflow from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    ...
```

1. load input data from the InfluxDB, accessing the output of a previous task specified in the class constructor using the `prev_input` option.
2. Process runs by grouping them in based on the LHC fill number. Only process the data once the fill has been completed.

Typically, the first task in a workflow will read data from DBS (a.k.a. DAS), either CMS prompt reco datasets or RAW files from calibration streams. It is important to notice that the automation framework is independent of the CMS DAQ and T0 processing (and vice-versa), therefore one has to pay attention to the synchronization between the two systems. Two particular aspects are crucial:

- **CondDB**: a reconstruction task might require synchronization with Conddb (condition database). In particular RAW files from calibration streams are usually processed and made available on EOS by T0 before the corresponding physics dataset for the same run have been fully processed in prompt reco. Often the RAW data are available even before calibration from dedicated system (e.g. laser corrections for ECAL) or the PCL (e.g. ECAL pedestals) are updated by the respective workflows. To ensure that the reconstruction happens with the correct conditions the workflow can accept **lock** functions that will prevent a run to be processed even if in status "new" unless a certain condition is met. The Conddb related lock functions are available [here](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#conddb-locks). 
  
- **Tier0**: data files from T0 of any data tier (RAW, PROMPT, EXPRESS, ALCARECO, etc) are copied to several sites with disk storage after being processed at T0. The information about each file is uploaded to DBS at regular intervals by the T0 software. For each run and dataset combination the information concerning the files that contain the data for that combination is uploaded several times during the T0 processing. In practice this means that files of a given dataset containing data from a given run might appear while jobs that will produce more of those files are still running at T0. This can lead to automation jobs running only on a partial dataset, if a task picks up a run before the processing at T0 for that run and dataset has been fully completed. A set of locks to prevent this issue are provided [here](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#tier0-locks).
  
An example of how to use the locks can be found [here](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/blob/online-phisym-reco/runreco.py):
  ```python
  #!/usr/bin/env python3
  import sys
  from ecalautoctrl import HTCHandlerByRunDBS, CondDBLockGT, T0ProcDatasetLock

  if __name__ == '__main__':
      laser_ped_lock = CondDBLockGT(records=['EcalLaserAPDPNRatiosRcd', 'EcalPedestalsRcd']) # (1)
      t0lock = T0ProcDatasetLock(dataset='/AlCaPhiSym', stage='Repack') # (2)
        
      handler = HTCHandlerByRunDBS(task='phisym-reco',
                                   dsetname='/AlCaPhiSym/*/RAW',
                                   locks=[laser_ped_lock, t0lock]) # (3)

      ret = handler()

      sys.exit(ret)
  ```

1. Create a lock to wait for a payload to be appended to the specified tags with a "since" time/run-number that is more recent than the run end time.
2. Create a lock to wait for T0 to fully process and copy to EOS the RAW data from the `/AlCaPhiSym` stream. 
3. Set the locks in the handler class.
