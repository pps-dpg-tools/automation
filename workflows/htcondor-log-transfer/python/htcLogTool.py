import logging
import argparse
import subprocess
import os
import sys

def handle_result(command, result):
    """
    Handle the result of a subprocess call
    """
    if result.returncode != 0:
        logging.error('Error running command: \'%s\'',command)
        result.stdout = result.stdout.decode() if result.stdout else 'No standard output'
        result.stderr = result.stderr.decode() if result.stderr else 'No standard error'
        logging.error('Standard output:\n%s',result.stdout)
        logging.error('Standard error:\n%s',result.stderr)
        sys.exit(1)


def summary(args):
    """
    Get a summary of the logs from HTCondor for a given user
    """

    logging.debug('Running summary')
    constraints = {
        'Idle': 'JobStatus == 1',
        'Running': 'JobStatus == 2',
        'Removed': 'JobStatus == 3',
        'Completed': 'JobStatus == 4',
        'Held': 'JobStatus == 5',
    }

    data = {}

    for constraint,condition in constraints.items():
        logging.debug(f'Getting count for {constraint}')

        command = f'condor_q {args.user} -constraint \"{condition}\" -format  \"%d\\n\" ClusterId'
        logging.debug(f'Running command: {command}')
        count = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        )
        handle_result(command,count)
        job_ids = count.stdout.decode().strip().split('\n')
        # remove '' from the set
        if '' in job_ids:
            job_ids.remove('')
        data[constraint] = len(job_ids)

    # Print a summary
    logging.info('Summary for user %s (Job status, number of jobs):',args.user)
    for constraint,count in data.items():
        logging.info('%s: %d',constraint,count)

def transfer(args):
    """
    Transfer logs from HTCondor for a given user
    """
    logging.debug('Running transfer')
    # Get the list of completed jobs
    condition = ' \"JobStatus == 4\"'
    command = f'condor_q {args.user} -constraint \"JobStatus == 4\" -format \"%d\" ClusterId -format \".%d\\n\" ProcId'
    logging.info(f'Running command: {command}')
    job_ids = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    handle_result(command,job_ids)
    job_ids = job_ids.stdout.decode().strip().split('\n')
    if '' in job_ids:
        job_ids.remove('')

    logging.debug('Cluster IDs: \n%s',job_ids)

    if len(job_ids) == 0:
        logging.info('No completed jobs found')
        return

    # Transfer the logs from the first n jobs
    command = 'condor_transfer_data ' + ' '.join(job_ids[:args.nJobs])
    logging.info(f'Running command: {command}')
    transfer = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    handle_result(command,transfer)

    # Remove the jobs from HTCondor
    if not args.no_remove:
        command = 'condor_rm ' + ' '.join(job_ids[:args.nJobs])
        logging.info(f'Running command: {command}')
        remove = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        )
        handle_result(command,remove)

def parse_arguments():
    parser = argparse.ArgumentParser(description='Handle HTCondor logs')
    parser.add_argument('--debug',action='store_true',default=False,help='Print debug messages')
    parser.add_argument('--user',required=True,help='Username for which logs are queried')
    subparsers = parser.add_subparsers(
        help='Available modes. Use <mode> -h for mode-specific help. Must be selected.',
        title='Modes',
        dest='mode',
        required=True
    )

    summary_parser = subparsers.add_parser('summary', help='Summary of logs from HTCondor')
    summary_parser.set_defaults(func=summary)

    transfer_parser = subparsers.add_parser('transfer', help='Transfer logs from HTCondor')
    transfer_parser.add_argument('--nJobs',type=int,default=300,help='Number of jobs to transfer')
    transfer_parser.add_argument('--no-remove',action='store_true',default=False,help='Do not remove the jobs from HTCondor')
    transfer_parser.set_defaults(func=transfer)

    return parser.parse_args()

if __name__ == '__main__':
    args = parse_arguments()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG,
                            format='[%(asctime)s - %(levelname)s] %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        logging.debug('Debug mode enabled')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='[%(asctime)s - %(levelname)s] %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')

    args.func(args)
    sys.exit(0)