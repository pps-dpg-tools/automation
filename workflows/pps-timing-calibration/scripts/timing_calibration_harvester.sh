#!/bin/bash

set -ex

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Error occurred during script execution"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM ERR

export HOME=/afs/cern.ch/user/p/ppsgit/

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh

set -E

cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Get the DB and campaign names
DB=$(echo "$TASK" | grep -oP "(?<=--db )[\w-_]+")
CAMPAIGN=$(echo "$TASK" | grep -oP "(?<=-c )[\w-_]+")

# Get the run number from the script arguments
RUN_NUMBER=$(echo "$TASK" | grep -oP "(?<=run_number:)\d+")

# Check if double peak correction is needed
DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME=timing-calibration-is-double-peak-correction-needed
IS_DOUBLE_PEAK_CORRECTION_NEEDED=$(ecalrunctrl.py --db $DB status --campaign $CAMPAIGN --runs $RUN_NUMBER --noformat | grep -oP "(?<=$DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME=)\w+")

# Run the job
OFILE_ROOT=ppsDiamondTiming_calibration_${RUN_NUMBER}.root
if [ "$IS_DOUBLE_PEAK_CORRECTION_NEEDED" == "True" ]
then
    cmsRun $WDIR/src/CalibPPS/TimingCalibration/test/DiamondCalibrationHarvester_cfg.py inputFiles=$INFILE globalTag=$GT tVsLsFilename=$EOSDIR/$OFILE_ROOT
else
    cmsRun $WDIR/src/CalibPPS/TimingCalibration/test/DiamondCalibrationHarvester_cfg.py inputFiles=$INFILE globalTag=$GT
fi

mkdir -p $EOSDIR
OFILE_SQLITE=ppsDiamondTiming_calibration_${RUN_NUMBER}.sqlite
if [ "$IS_DOUBLE_PEAK_CORRECTION_NEEDED" == "True" ]
then
    mv $EOSDIR/$OFILE_SQLITE $EOSDIR/ppsDiamondTiming_calibration_without_dp_correction_${RUN_NUMBER}.sqlite
    mv $EOSDIR/$OFILE_ROOT $EOSDIR/ppsDiamondTiming_calibration_without_dp_correction_${RUN_NUMBER}.root
fi
mv ppsDiamondTiming_calibration.sqlite $EOSDIR/$OFILE_SQLITE
mv DQM*root $EOSDIR/$OFILE_ROOT

ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE_SQLITE},${EOSDIR}/${OFILE_ROOT}"
