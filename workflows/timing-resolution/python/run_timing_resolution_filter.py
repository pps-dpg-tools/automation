import argparse
import logging
import sys

from typing import Any, Dict, List, Set

from ecalautoctrl.RunCtrl import RunCtrl


class TimingResolutionRunFilter:
    """
    Tasks using this handler (and its derivatives) should be executed before any tasks for possible skipping as only tasks in state 'new' are updated.
    """

    def __init__(self,
                 tasks_for_skipping: List[str],
                 min_rp_time_ready: int,
                 min_rp_sect_45_ready: int,
                 min_rp_sect_56_ready: int):
        self.tasks_for_skipping = tasks_for_skipping
        self.min_rp_time_ready = min_rp_time_ready
        self.min_rp_sect_45_ready = min_rp_sect_45_ready
        self.min_rp_sect_56_ready = min_rp_sect_56_ready

        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--db',
                            dest='dbname',
                            type=str,
                            help='Database name.',
                            required=True)
        self.parser.add_argument('--campaign',
                            dest='campaign',
                            type=str,
                            help='Processing campaign.',
                            required=True)

        # create the basic streamer logger
        self.stream_handler = logging.StreamHandler()
        self.stream_handler.setLevel(logging.INFO)
        self.stream_handler.setFormatter(logging.Formatter(f'[{self.__class__.__name__}]: %(message)s'))
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger()
        self.log.addHandler(self.stream_handler)
        # remove default logger
        self.log.handlers.pop(0)

    def __call__(self) -> int:
        """
        Check tasks for possible skipping which are in state 'new' and are inserted into any of the workflows,
        and mark them as 'skipped' if they don't meet some condition(s).
        """
        args = self.parser.parse_args()
        rctrl = RunCtrl(dbname=args.dbname, campaign=args.campaign)

        active_tasks: Set[str] = {active_task for task in rctrl.getWorkflows().values() for active_task in task['active']}
        active_tasks_for_skipping: Set[str] = set(self.tasks_for_skipping).intersection(active_tasks)
        runs: List[Dict[str, Any]] = rctrl.getRuns(status={task: 'new' for task in active_tasks_for_skipping}, active=True)
        for run in runs:
            run_number = run['run_number']
            if self.should_skip_run(run_number, rctrl):
                rctrl.updateStatus(run=run_number, status={task: 'skipped' for task in active_tasks_for_skipping})
                self.log.info(f'Run {run_number} not meeting the criteria. Skipped for tasks: {active_tasks_for_skipping}.')
            else:
                self.log.info(f'Run {run_number} meeting the criteria and not skipped.')

        return 0

    def should_skip_run(self, run_number, rctrl) -> bool:
        query = rctrl.oms.query('lumisections').filter('run_number', run_number).filter('cms_active', True)
        ls_attributes = query.paginate(page=1, per_page=100000).data().json()['data']

        total_rp_time_ready: int = len([ls for ls in ls_attributes if ls['attributes']['rp_time_ready']])
        total_rp_sect_45_ready: int = len([ls for ls in ls_attributes if ls['attributes']['rp_sect_45_ready']])
        total_rp_sect_56_ready: int = len([ls for ls in ls_attributes if ls['attributes']['rp_sect_56_ready']])

        return (total_rp_time_ready < self.min_rp_time_ready or
                total_rp_sect_45_ready < self.min_rp_sect_45_ready or
                total_rp_sect_56_ready < self.min_rp_sect_56_ready)

if __name__ == '__main__':
    run_filter = TimingResolutionRunFilter(tasks_for_skipping=['timing-resolution-worker',
                                                               'timing-resolution-harvester',
                                                               'timing-resolution-iteration'],
                                           min_rp_time_ready=100,
                                           min_rp_sect_45_ready=100,
                                           min_rp_sect_56_ready=100)

    ret = run_filter()
    sys.exit(ret)
