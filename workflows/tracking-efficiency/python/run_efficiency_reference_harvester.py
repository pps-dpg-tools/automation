import sys
from ecalautoctrl import HTCHandlerByRun
        
if __name__ == '__main__':
    handler = HTCHandlerByRun(task='tracking-efficiency-reference-harvester',
                              prev_input='tracking-efficiency-reference-worker',
                              deps_tasks=['tracking-efficiency-reference-worker'])

    ret = handler()
    
    sys.exit(ret)
