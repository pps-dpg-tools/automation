#!/bin/bash

set -ex

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Error occurred during script execution"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM ERR

source /home/ppsgit/setup.sh

export HOME=/afs/cern.ch/user/p/ppsgit/

# This is needed for omsapi to work
# False ^
# kinit ppsgit -k -t ppsgit.keytab

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh

set -E

cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Get the run number from the script arguments
run_number=$(echo "$TASK" | grep -oP "(?<=run_number:)\d+")

# Get the DB and campaign names
DB=$(echo "$TASK" | grep -oP "(?<=--db )[\w-_]+")
CAMPAIGN=$(echo "$TASK" | grep -oP "(?<=-c )[\w-_]+")

# Get the current iteration number
# TODO: we could inject this value based on the timing resolution iteration task Python file
ITERATION_TASK_DB_FIELD_NAME=timing-resolution-iteration-value
ITERATION=$(ecalrunctrl.py --db $DB status --campaign $CAMPAIGN --runs $run_number --noformat | grep -oP "(?<=$ITERATION_TASK_DB_FIELD_NAME=)\d+")

# Set the previous iteration number
LAST_ITERATION=$((ITERATION-1))

# Get the calib files from previous iterations
CALIB_FILE_LIST=""

for ((i=0; i<=$LAST_ITERATION; i++))
do
    # Check if the file exists
    if [ -e "$EOSDIR/calibOutputTimingResolutionHarvester_run${run_number}_iteration${i}.json" ]
    then
        # Append the filename to the list
        if [ -z "$CALIB_FILE_LIST" ]
        then
            CALIB_FILE_LIST="$EOSDIR/calibOutputTimingResolutionHarvester_run${run_number}_iteration${i}.json"
        else
            CALIB_FILE_LIST="${CALIB_FILE_LIST},$EOSDIR/calibOutputTimingResolutionHarvester_run${run_number}_iteration${i}.json"
        fi
    fi
done

MODIFIED_INFILE=$(echo "$INFILE" | sed 's,/eos,file:/eos,g')

# Timing calibration workflow campaign name for the sqlite file
CALIB_CAMPAIGN_NAME=2024_double_peak_correction_v10

# Run the job
if [ $ITERATION == 0 ]
then
    cmsRun $WDIR/src/Analyzer/DiamondTimingAnalyzer/python/harvester.py globalTag=$GT rootFiles=$MODIFIED_INFILE sqlFileName=sqlite_file:/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/dev/pps-test-timing-calibration/$CALIB_CAMPAIGN_NAME/${run_number}/ppsDiamondTiming_calibration_${run_number}.sqlite

else                                                                                                                                                              # calibInput from the harvester (last iteration) matching the one in the worker
    cmsRun $WDIR/src/Analyzer/DiamondTimingAnalyzer/python/harvester.py globalTag=$GT rootFiles=$MODIFIED_INFILE loopIndex=$ITERATION calibFiles=$CALIB_FILE_LIST calibInput=$EOSDIR/calibOutputTimingResolutionHarvester_run${run_number}_iteration${LAST_ITERATION}.json
fi

mkdir -p $EOSDIR
OFILE=outputTimingResolutionHarvester_run${run_number}_iteration${ITERATION}.root
OFILE2=calibOutputTimingResolutionHarvester_run${run_number}_iteration${ITERATION}.json
mv DQM*$run_number*.root $EOSDIR/$OFILE
mv calib.json $EOSDIR/$OFILE2

ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE}" "calibOutput:${EOSDIR}/${OFILE2}"
