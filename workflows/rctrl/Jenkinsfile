// Check out the shared libs from the branch or tag of the workflow
def shared_lib = "pps-shared-libs@${scm.branches[0].name}"
echo "Using sharedlib version: $shared_lib"
// Import the env variables defined in the shared lib
library("$shared_lib").ppsctrl.vars.GlobalVars.setenv(this, env.JOB_BASE_NAME)

// Use this campaign only in dev mode
if (!(env.JOB_BASE_NAME ==~ /.*-prompt$/ || env.JOB_BASE_NAME ==~ /.*-repro$/)) {
    campaign = "2024_track_plots_v1"
}

pipeline {
    agent {
        label 'lxplus-pps'
    }
    triggers {
        cron('@hourly')
    }
    stages {
        stage('check-complete') {
            steps {
                // You can add '--notify=$notify_url' if you have properly setup the url in automation/shared-lib/src/ectrl/vars/GlobalVars.groovy
                automationApptainerStep "ecalrunctrl.py --db=$dbinstance --notify=$notify_url close -c all"
            }
        }
        // Fetch only if we are running in prompt or repro mode
        stage('fetch-if-needed'){
            steps{
                script {
                    if (env.JOB_BASE_NAME ==~ /.*-prompt$/) {
                        // You can add '--notify=$notify_url' if you have properly setup the url in automation/shared-lib/src/ectrl/vars/GlobalVars.groovy
                        // This keeps the runs to process in sync with OMS - should be disabled for the reprocessing
                        automationApptainerStep "ecalrunctrl.py --db=$dbinstance --notify=$notify_url sync -c $campaign"
                    }
                }
            }
        }
        // Fail evicted idle jobs
        stage('fail-evicted-idle-jobs'){
            steps {
                automationApptainerStep "ecalrunctrl.py --db=$dbinstance fail-evicted-jobs -c $campaign --status=idle --condor-q-status=idle --hour-threshold=2"
            }
        }
    }
    post {
        unsuccessful {
            sendMattermostNotification "ERROR"
        }
    }
}
