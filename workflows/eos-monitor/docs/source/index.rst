PPS Automation workflow: EOS monitor
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

monitor.py script
======================================

.. argparse::
   :filename: ../monitor.py
   :func: makeParser
   :prog: monitor.py

write_config.py script
======================================

.. argparse:: 
   :filename: ../write_config.py
   :func: makeParser
   :prog: write_config.py
