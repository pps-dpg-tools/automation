import sys
import subprocess
from os import path
from typing import Optional, List, Dict
from itertools import islice


from ecalautoctrl import HTCHandler,process_by_run,QueryDBS,JobCtrl
from ecalautoctrl.TaskHandlers import get_files_prev_task

@process_by_run
class ReferenceAnalysisHandlerDBS(HTCHandler):
    """Works as an HTCHandler that always gets the same run,
    and gets an input from a previous workflow.
    """
    def __init__(self,
                task: str,
                dsetname: str,
                ref_runs: List[str],
                prev_input: str,
                deps_tasks: Optional[List[str]]=None,
                **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname
        self.ref_runs = ref_runs
        self.prev_input = prev_input

    def get_files_dbs(self, process_runs: List[Dict],reference_runs: List[str]) -> List[str]:
        """
        Retrieve file names for given runs from DBS.
        This function is added by :deco:`~ecalautoctrl.dbs_data_source`.

        :param process_runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
        This are the runs for which the efficiency computation is made.
        :param reference_runs: list of runs to be used as reference dataset.
        """
        flist = []
        try:
            if self.dsetname:
                for run in process_runs:
                    _,pd,sd,tier = self.dsetname.split('/')

                    # add era to dataset name, validating it in the process
                    # sd = '*' + run["era"] + sd
                    # skipping this, because the era of the run being processed might not match
                    # the era of the reference data

                    fullname = f'/{pd}/{sd}/{tier}'
                    fromt0 = self.opts.t0 if 't0' in self.opts else False
                    dataQuery = QueryDBS(dataset=fullname)
                    for ref_run in reference_runs:
                        flist.extend(dataQuery.getRunFiles(run=int(ref_run), fromt0=fromt0))
            return flist
        except AttributeError as ae:
            raise AttributeError('Please define self.dsetname in your class when using the dbs_data_source decorator')

    def submit(self):
        """
        Submit runs in status task: new.
        Resubmit runs in status task: reprocess.
        Mainly from HTCHandler.submit (need to edit the condor arguments)
        """

        for group in self.groups():
            # get efficiency input file
            prev_output = get_files_prev_task(self,group)
            effInput = prev_output[0]

            # master run
            run = group[-1]
            # Get data from DBS for reference dataset
            fdict = self.get_files_dbs(process_runs=group,reference_runs=self.ref_runs)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr = (run[self.task] == 'reprocess')
                if jctrl.taskExist() and run[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        allow_repr = False
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    # split the file list into groups accordingly to nfiles,
                    # apexes are added around join(f) to ensure condor will later
                    # interpret the comma separated list as a single item
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    self.opts.nfiles = len(fdict) if self.opts.nfiles==-1 else self.opts.nfiles
                    it = iter(fdict)
                    flist = [",".join([f for f in ff]) for ff in iter(lambda: tuple(islice(it, self.opts.nfiles)), ())]
                    with open('args.txt', 'w') as ff:
                        ff.write('\n'.join([f for f in flist]))
                    # get the global tag from the influxdb.
                    # if not set leave GT empty (rely on auto cond).
                    gt = run['globaltag'] if 'globaltag' in run else '\'\''
                    self.log.info(f'GlobalTag: {gt}')
                    ret = subprocess.run(['condor_submit',
                                        f'{self.opts.template}',
                                        '-spool',
                                        '-queue', '1 fname from args.txt',
                                        '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {effInput}"',
                                        '-append', 'max_retries = 0'],
                                        capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - log file
                        jctrl.createTask(jids=list(range(len(flist))),
                                        recreate=allow_repr,
                                        fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                'inputs': f,
                                                'log': f'{logurl}/{self.task}-{cluster}-{i}.log'} for i,f in enumerate(flist)])
                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        """

        runs = self.rctrl.getRuns(status={self.task: 'processing'})


        for run_dict in runs:
            # get efficiency input file
            prev_output = get_files_prev_task(self,runs)
            effInput = prev_output[0]

            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # actually running in condor.
            for jid in jctrl.getRunning():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)
            # resubmit failed
            if not jctrl.taskExist() or jctrl.getFailed() == []:
                self.log.info(f'no failures found for run {run}')
            else:
                self.log.info(f'found failed job for run {run}')
                # Building the resubmission arguments
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                # move to AAA if file can't be read from T0 eos.
                flist = []
                for i in jctrl.getFailed():
                    inputs = jctrl.getJob(i, last=True)[0]['inputs']
                    if '/eos/cms/tier0' in inputs and not all([path.isfile(p) for p in inputs.split(',')]):
                        inputs = inputs.replace('file:/eos/cms/tier0', 'root://cms-xrd-global.cern.ch/')
                    flist.append(i+', '+inputs)
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                with open('args.txt', 'w') as ff:
                    ff.write('\n'.join(flist))
                # get the global tag from the influxdb.
                # if not set leave GT empty (rely on auto cond).
                gt = run_dict['globaltag'] if 'globaltag' in run_dict else '\'\''
                self.log.info(f'GlobalTag: {gt}')
                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'jobid, fname from args.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(jobid) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {effInput}"',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"',
                                      '-append', 'max_retries = 0'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    # set the job status to idle
                    for i in jctrl.getFailed():
                        jctrl.idle(jid=i)
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0

@process_by_run
class ReferenceAnalysisHandler(HTCHandler):
    """Works as an HTCHandler that always gets the same run,
    and gets an input from a previous workflow.
    """
    def __init__(self,
                task: str,
                ref_files: List[str],
                prev_input: str,
                deps_tasks: Optional[List[str]]=None,
                **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.ref_files = ref_files
        self.prev_input = prev_input

    def submit(self):
        """
        Submit runs in status task: new.
        Resubmit runs in status task: reprocess.
        Mainly from HTCHandler.submit (need to edit the condor arguments)
        """

        for group in self.groups():
            # get efficiency input file
            prev_output = get_files_prev_task(self,group)
            effInput = prev_output[0]

            # master run
            run = group[-1]

            # Get data from local files. Prepend 'file:' tag
            fdict = [ f'file:{f}' for f in self.ref_files]

            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr = (run[self.task] == 'reprocess')
                if jctrl.taskExist() and run[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        allow_repr = False
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    # split the file list into groups accordingly to nfiles,
                    # apexes are added around join(f) to ensure condor will later
                    # interpret the comma separated list as a single item
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    self.opts.nfiles = len(fdict) if self.opts.nfiles==-1 else self.opts.nfiles
                    it = iter(fdict)
                    flist = [",".join([f for f in ff]) for ff in iter(lambda: tuple(islice(it, self.opts.nfiles)), ())]
                    with open('args.txt', 'w') as ff:
                        ff.write('\n'.join([f for f in flist]))
                    # get the global tag from the influxdb.
                    # if not set leave GT empty (rely on auto cond).
                    gt = run['globaltag'] if 'globaltag' in run else '\'\''
                    self.log.info(f'GlobalTag: {gt}')
                    ret = subprocess.run(['condor_submit',
                                        f'{self.opts.template}',
                                        '-spool',
                                        '-queue', '1 fname from args.txt',
                                        '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {effInput}"',
                                        '-append', 'max_retries = 0'],
                                        capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - log file
                        jctrl.createTask(jids=list(range(len(flist))),
                                        recreate=allow_repr,
                                        fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                'inputs': f,
                                                'log': f'{logurl}/{self.task}-{cluster}-{i}.log'} for i,f in enumerate(flist)])
                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        """

        runs = self.rctrl.getRuns(status={self.task: 'processing'})


        for run_dict in runs:
            # get efficiency input file
            prev_output = get_files_prev_task(self,runs)
            effInput = prev_output[0]

            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # actually running in condor.
            for jid in jctrl.getRunning():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)
            # resubmit failed
            if not jctrl.taskExist() or jctrl.getFailed() == []:
                self.log.info(f'no failures found for run {run}')
            else:
                self.log.info(f'found failed job for run {run}')
                # Building the resubmission arguments
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                # move to AAA if file can't be read from T0 eos.
                flist = []
                for i in jctrl.getFailed():
                    inputs = jctrl.getJob(i, last=True)[0]['inputs']
                    if '/eos/cms/tier0' in inputs and not all([path.isfile(p) for p in inputs.split(',')]):
                        inputs = inputs.replace('file:/eos/cms/tier0', 'root://cms-xrd-global.cern.ch/')
                    flist.append(i+', '+inputs)
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                with open('args.txt', 'w') as ff:
                    ff.write('\n'.join(flist))
                # get the global tag from the influxdb.
                # if not set leave GT empty (rely on auto cond).
                gt = run_dict['globaltag'] if 'globaltag' in run_dict else '\'\''
                self.log.info(f'GlobalTag: {gt}')
                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'jobid, fname from args.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(jobid) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt} {effInput}"',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"',
                                      '-append', 'max_retries = 0'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    # set the job status to idle
                    for i in jctrl.getFailed():
                        jctrl.idle(jid=i)
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0



if __name__ == '__main__':

    ref_files = [
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_0.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_1.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_10.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_100.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_101.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_102.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_103.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_104.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_105.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_106.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_107.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_108.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_109.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_11.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_110.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_111.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_112.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_113.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_114.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_115.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_116.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_117.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_118.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_119.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_12.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_120.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_121.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_122.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_123.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_124.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_125.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_126.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_127.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_128.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_129.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_13.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_130.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_131.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_132.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_133.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_134.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_135.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_136.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_137.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_138.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_139.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_14.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_140.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_141.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_142.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_143.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_144.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_145.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_146.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_147.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_148.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_149.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_15.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_150.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_151.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_152.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_153.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_154.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_16.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_17.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_18.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_19.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_2.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_20.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_21.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_22.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_23.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_24.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_25.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_26.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_27.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_28.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_29.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_3.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_30.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_31.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_32.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_33.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_34.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_35.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_36.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_37.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_38.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_39.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_4.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_40.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_41.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_42.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_43.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_44.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_45.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_46.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_47.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_48.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_49.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_5.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_50.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_51.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_52.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_53.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_54.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_55.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_56.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_57.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_58.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_59.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_6.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_60.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_61.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_62.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_63.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_64.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_65.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_66.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_67.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_68.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_69.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_7.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_70.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_71.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_72.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_73.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_74.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_75.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_76.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_77.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_78.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_79.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_8.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_80.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_81.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_82.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_83.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_84.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_85.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_86.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_87.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_88.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_89.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_9.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_90.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_91.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_92.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_93.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_94.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_95.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_96.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_97.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_98.root',
'/eos/project-c/ctpps/subsystems/Pixel/RPixTracking/EfficiencyCalculation2023/v1/ReferenceSample/PPS_ALCARECO_366933_99.root',
    ]

    handler = ReferenceAnalysisHandler(task='re-tracking-efficiency-reference-worker',
                                       ref_files=ref_files,
                                       prev_input='re-tracking-efficiency-analysis-harvester',
                                       deps_tasks=['re-tracking-efficiency-analysis-harvester'])

    ret = handler()

    sys.exit(ret)
