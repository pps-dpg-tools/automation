import json
import os
import sys

from typing import List

from ecalautoctrl import IterationBaseHandler

JSON_FILENAME: str = 'calibOutputTimingResolutionHarvester_run{run_number}_iteration{iteration_number}.json'
NUM_OF_PARAMS: int = 4
ZERO_PARAMS: List[str] = ['0' for _ in range(NUM_OF_PARAMS)]


class IterationByTimePrecisionDelta(IterationBaseHandler):
    """
    Iteration handler with a stop condition based on a percent of channels (*channels_threshold*)
    where the difference between the time precision from the previous and current iteration is less
    than *time_precision_delta*. Additionally, at least *min_iteration* are executed for this check
    to even be considered, and no more than *max_iteration* are performed.

    :param task: task name.
    :param deps_tasks: list of workfow dependencies.
    :param tasks_to_iterate: list of tasks which might need further iteration.
    :param min_iteration: minimum iteration to check the time precision delta.
    :param max_iteration: maximum iteration to check the time precision delta.
    :param time_precision_delta: required difference in the time precision between channels (in nanoseconds).
    :param channels_threshold: minimum percentage of channels meeting the delta.
    """
    def __init__(self,
                 task: str,
                 deps_tasks: List[str],
                 tasks_to_iterate: List[str],
                 min_iteration: int,
                 max_iteration: int,
                 time_precision_delta: float,
                 channels_threshold: float,
                 **kwargs):
        assert min_iteration > 0, 'At least two JSON files are required to check the delta.'
        super().__init__(task=task, deps_tasks=deps_tasks, tasks_to_iterate=tasks_to_iterate, **kwargs)
        self.min_iteration: int = min_iteration
        self.max_iteration: int = max_iteration
        self.time_precision_delta: float = time_precision_delta
        self.channels_threshold: float = channels_threshold

        self.parser.add_argument('--eosdir',
                                 dest='eosdir',
                                 type=str,
                                 required=True,
                                 help='Base path of output location on EOS')

    def should_iterate(self) -> bool:
        if self.curr_iteration >= self.max_iteration:
            self.log.info(f'Max iterations reached for Run {self.run_number}.')
            return False

        if self.curr_iteration >= self.min_iteration:
            json_directory: str = os.path.join(self.opts.eosdir, self.run_number)
            with open(os.path.join(json_directory, JSON_FILENAME.format(run_number=self.run_number, iteration_number=self.curr_iteration - 1))) as prev_json_file:
                prev_json = json.loads(prev_json_file.read())
            with open(os.path.join(json_directory, JSON_FILENAME.format(run_number=self.run_number, iteration_number=self.curr_iteration))) as curr_json_file:
                curr_json = json.loads(curr_json_file.read())

            working_channels: int = 0
            channels_below_delta: int = 0
            for prev_sector, curr_sector in zip(prev_json['Parameters']['Sectors'], curr_json['Parameters']['Sectors']):
                for prev_station, curr_station in zip(prev_sector['Stations'], curr_sector['Stations']):
                    for prev_plane, curr_plane in zip(prev_station['Planes'], curr_station['Planes']):
                        for prev_channel, curr_channel in zip(prev_plane['Channels'], curr_plane['Channels']):
                            prev_channel_params: List[str] = prev_channel['param']
                            if len(prev_channel_params) == NUM_OF_PARAMS and prev_channel_params != ZERO_PARAMS:
                                working_channels += 1
                                if abs(float(prev_channel['time_precision']) - float(curr_channel['time_precision'])) < self.time_precision_delta:
                                    channels_below_delta += 1

            self.log.info(f'Channels below delta: {channels_below_delta}. Working channels: {working_channels}.')
            return channels_below_delta / working_channels <= self.channels_threshold

        self.log.info(f'Min iterations not reached yet for Run {self.run_number}.')
        return True

if __name__ == '__main__':
    handler = IterationByTimePrecisionDelta(task='timing-resolution-iteration',
                                            deps_tasks=['timing-resolution-harvester'],
                                            tasks_to_iterate=['timing-resolution-worker', 'timing-resolution-harvester'],
                                            min_iteration=4,
                                            max_iteration=10,
                                            time_precision_delta=0.01,
                                            channels_threshold=0.95)

    ret = handler()
    sys.exit(ret)
