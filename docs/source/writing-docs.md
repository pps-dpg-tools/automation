# Writing documentation

Proper code documentation is just as essential as the code itself. Both the PPS Automation Framework as a whole and the calibration workflows should **always** have up-to-date documentation.

In particular, the documentation of each calibration workflow should cover these topics:

- Description of the workflow functionality.
- Instructions to run the workflow manually.
- Instructions to run the workflow in the automation framework. 
- Code documentation (if applicable).

A folder called `docs` in the workflows/&lt;workflow&gt; directory has to be created, and a MarkDown file named `README.md` file must be added to it. Furthermore, to generate code documentation using [Sphinx](https://www.sphinx-doc.org/en/master/), the user should create, within the `docs` folder, the following files:

- Makefile
- source/conf.py
- source/index.rst

They can be created following the example of the [**cleaner**](https://gitlab.cern.ch/pps-dpg-tools/automation/-/tree/master/workflows/cleaner) workflow.

The user should also add the documentation of the new workflow to the [global navigation menu](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docs/mkdocs.yml#L45), using the other workflows as example. 

Writing documentation locally makes the process much easier and more efficient. Some instructions to do that are provided below.

## Local documentation development

The easiest way to develop the documentation is by cloning the automation repository to a local area and building the documentation locally. This allows to rapidly test any changes and visually verify the results before deploying the final version. 

The only prerequisite is the availability of a python3.9 interpreter, which is used in the production setup and was used to test these instructions (other versions might work as well). Setup the local area following the commands below:

- Clone the PPS automation repository:
```bash
git clone https://gitlab.cern.ch/pps-dpg-tools/automation.git
cd automation
```
- Create a virtual environment and activate it:
```bash
python3.9 -m venv .venv
source .venv/bin/activate
```
- Install the necessary dependencies:
```bash
pip install --upgrade pip setuptools wheel
pip install mkdocs mkdocs-material mkdocs-material-extensions mkdocs-static-i18n mkdocs-diagrams sphinx sphinx_rtd_theme sphinx-argparse sphinx-autodoc-typehints sphinx-toolbox
git clone https://gitlab.cern.ch/cmsoms/oms-api-client.git
# NICE credentials will be required at this point. Don't copy-paste the whole block.
cd oms-api-client
python -m pip install -I .
python -m pip install -I influxdb
python -m pip install -I dbs3-client==3.17.0
cd -
python -m pip install git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/ --no-deps -I
```
- Build and serve the documentation:
```bash
mkdocs serve --no-livereload -f docs/mkdocs.yml
```
- View the freshly-built result by pointing your browser to: [http://127.0.0.1:8000](http://127.0.0.1:8000)