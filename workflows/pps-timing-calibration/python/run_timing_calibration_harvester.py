import sys

from ecalautoctrl import HTCHandlerByRun


if __name__ == '__main__':
    handler = HTCHandlerByRun(task='timing-calibration-harvester',
    			              prev_input='timing-calibration-worker',
                              deps_tasks=['timing-calibration-worker'])

    ret = handler()
    sys.exit(ret)
