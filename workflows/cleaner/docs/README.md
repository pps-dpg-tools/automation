# Cleaner

This workflow takes care of deleting the outputs/logs from tasks, once the runs are completed. 

The list of campaigns, tasks, and fields containing the file paths to delete are defined in [**to_remove.py**](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/workflows/cleaner/to_remove.py).
Further code docs can be found [**here**](build/html/index.html).

## Instructions for manual execution

Similarly to what is done in the Jenkinsfile, to run the cleaner manually the user needs to:

- [Connect to the docker image](#using-the-automation-image)
- Remember to export the DB credentials (ask an admin!):<br>
```bash
export ECAL_INFLUXDB_USER=user
export ECAL_INFLUXDB_PWD=password
source /home/ppsgit/setup.sh
```
- Authenticate via kerberos as a user with write access to the files that will be deleted:<br>
```bash
kinit NICE_user
```
- Run the cleaner: 
```bash
cd workflows/cleaner
python3 cleaner.py
```

For obvious reasons, the cleaner cannot be run locally on a user machine.