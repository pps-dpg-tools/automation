to_remove = {
    're_tracking_efficiency_v1': {
        'tracking-efficiency-alcareco' : ['output'],
    },
    're_tracking_efficiency_v2_test': {
        're-tracking-efficiency-analysis-worker-with-reco' : ['output'],
        're-tracking-efficiency-reference-worker' : ['output'],
    }
}
"""
**Structure**
::

    to_remove = {
         'campaign_for_removal' : {
             'task_for_removal' : ['job fields to remove',...],
         }
    }
"""
