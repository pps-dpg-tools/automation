[ecalautoctrl-git]: https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/
[ecalautoctrl-docs]: https://cmsecaldocs.web.cern.ch/ecalautoctrl/
[pps-automation-git]: https://gitlab.cern.ch/pps-dpg-tools/automation


# Code snippets and instructions

This section serves the purpose of collecting code snippets and instructions for various operations. Other sections of the documentation refer to this part.

The complete documentation of the [ecalautoctrl python package][ecalautoctrl-git] (automation-control) can be found at this [link][ecalautoctrl-docs].
The package provides both the tools to control the automation operation and those for accessing information like the automation status and outputs.

## Using the automation image

Docker images are maintained with all the necessary software to interact with the automation framework. The images work with singularity and can be either built from the CERN gitlab registry or an unpacked version can be accessed from cvmfs. The first method works everywhere (lxplus, laptop, etc) the second one requires having cvmsf mounted (out of the box on lxplus, for usage on a personal machine see [the cvmfs guide](https://cvmfs.readthedocs.io/en/latest/cpt-quickstart.html#docker-container)).

To build and run an interactive shell using the image (e.g. dev version) on the gitlab registry use:

```bash
export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity"
singularity shell --cleanenv -B /afs -B /eos -B /cvmfs docker://gitlab-registry.cern.ch/pps-dpg-tools/automation:dev
```

To access the unpacked image (e.g. dev version) on cvmfs:

```bash
singularity shell --cleanenv -B /afs -B /cvmfs -B /eos /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/pps-dpg-tools/automation:dev
```

To access the unpacked image (e.g. dev version) on eos (**recommended**):

```bash
singularity shell --cleanenv -B /afs -B /cvmfs -B /eos /eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/unpacked-images/automation:dev
```


In all cases the automation environment must be activated following [these instructions](#launching-data-processings)

??? note "Mounting EOS and AFS"
    Mounting EOS is optional, `-B /eos` can be omitted. Likewise `-B /afs` will attach AFS to the image, this is optional too.

## Checking the docker image

Whenever you submit a change that needs the automation docker image to be rebuilt (e.g. change to the CMSSW installation, new ecalautoctrl feature), after a push is made to the master branch, you will need to wait for the *dev* image to be re-built and unpacked on cvmfs. This can take a bit of time (O(15 min)). To know when it's ready, mark the time at the start of the build and monitor the docker image creation time with:<br>
`ls -la /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/pps-dpg-tools/automation:dev`

## Launching data processings

In the following I list the typical actions necessary to launch a data processing after connecting to the automation image.

### Setup the environment

Run the following commands to setup your environment. Ask an admin the credentials if you don't have them!

```bash
export USER=`whoami`
export ECAL_INFLUXDB_USER=user
export ECAL_INFLUXDB_PWD=password
source "/home/ppsgit/setup.sh"
```

If you need to access eos or submit jobs from the image, you will also need to:

```bash
export KRB5CCNAME=/tmp/krb5cc_$UID
kinit $USER
```

### Creating a new campaign

A new processing campaign can be created using the `ecalrunctrl.py` script.

```bash
ecalrunctrl.py --db 'db_name' create --campaign 'test_rereco'
```

`db_name` is the name of the influx database in use (usually one per subdetector with versioning and separation between prompt and rereco). `test_rereco` is the name of the new campaign. Further options are described in the script help menu `ecalrunctrl.py create --help`.

### Adding tasks to the campaign

Run types allow to group a list of workflows. This allows for example to specify different filters for each group.

By default the campaign is created with a single run type `all` that filters run by rejecting non global runs. The run types can be customized using the `rtype-create`, `rtype-update` and `rtype-list` sub-commands of the `ecalrunctrl.py` script.

To create a new run type with its workflows:

```bash
ecalrunctrl.py --db 'db_name' rtype-create --campaign 'test_rereco' --type 'new_type' --add task1,task2
```

The `--db` and `--campaign` options match the one specified at creation time. The `new_type` run type is added with two tasks (comma separated argument to the `--add` option).

You can also use the `--copy` option to specify a run type from which you want to copy the list of workflows.

Tasks are added to the newly created campaign with:

```bash
ecalrunctrl.py --db 'db_name' rtype-update --campaign 'test_rereco' --add task1,task2,task3 --type=all
```

The default `all` run type is updated adding three tasks (comma separated argument to the `--add` option).

All runs injected in the future in the `test_rereco` campaign will be processed by the three tasks.

!!! note
    The task name should match the one specified with the `task` argument of the [TaskHandler class](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.HandlerBase).
    Both the git branch name and the Jenkins' item name assocaited to the workflow are irrelevant.
    For example to add the [tracking-efficiency-analysis-worker](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/workflows/tracking-efficiency/python/run_efficiency_analysis_worker.py) task the above command line should include `--add tracking-efficiency-analysis-worker`.

Both the options above allow to specify a run registry filter using the `--rr-filter` option which overrides the current filter in the specific run type.

```bash
ecalrunctrl.py --db 'db_name' rtype-update --campaign 'test_rereco' --add task1,task2,task3 --type=all --rr-filter '[{"attribute_name": "sequence", "operator": "EQ", "value": "GLOBAL-RUN"}, {"attribute_name": "last_lumisection_number", "operator": "GE", "value": 100}, {"attribute_name": "stable_beam", "operator": "EQ", "value": "true"}]'
```

This will add two additional filters to the default one (select only global runs) existing in the `all` run type: only runs whose last lumisection number is greater or equal to 100 and are part of stable beams will be injected for processing by the three defined tasks.

To list all run types of some campaign and their filters:

```bash
ecalrunctrl.py --db 'db_name' rtype-list --campaign 'test_rereco'
```

You can also use the `--type` option to specify a specific run type to be listed.

### Injecting runs in the new campaign

Once the new campaign has been created and tasks have been added, data can be injected for processing using the command below.

```bash
ecalrunctrl.py --db 'db_name' inject --campaign 'test_rereco' --era 'Run2022D' --globaltag='MYGT'
```

The `--db` and `--campaign` options match the one specified at creation time. Data can be injected with the granularity of a single run. Options to inject a single run or group of runs (run ranges, era) are available:

```
--runs RUNS           Comma separated list of
                        run(s) to inject in the new
                        campaign.
--range RRANGE        Define a range of runs to be
                        injected (min,max).
--era ERA             Inject all runs belonging to
                        a given CMS data acquisition
                        era.
```

### Marking runs for reprocessing

Similarly to injection, runs can be marked for reprocessing manually with the command below.

```bash
ecalrunctrl.py --db 'pps_test_v1' reprocess --campaign 'test_rereco' --runs 366504 --tasks tracking-efficiency
```

The `--tasks` option defines which tasks should be marked for reprocessing.

## Manually testing a workflow

!!! Important
    For the following steps the [GRID Proxy Certificates](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid#ObtainingCert) are needed.

    You should have your user certificate saved in `$HOME/.globus/usercert.pem`<br>
    You should have an rsa-encripted certificate in `$HOME/.globus/rsauserkey.pem`. You can create it from your .p12 certificate with <br>
    `openssl pkcs12 -in myCertificate.p12 -nocerts -out userkey.pem` <br>

    **Create your proxy certificate** and **remember its path**. <br>
    Use `voms-proxy-init --rfc --voms cms -valid 192:00 --out $HOME/grid_proxy.x509`<br>
    Creating it while connected to the docker image is not possible thus it must be done before.

Assuming that the workflow has been implemented and the user has access to the automation docker-image that was built for its *dev-\** branch, this is how a workflow can manually be tested:

1. Open a singularity shell in the image following [the instructions above](#using-the-automation-image).
2. Clone the automation repository (or `cd` to your development area).
3. Setup the environment (with eos/job submission access rights), create a test campaign (the one that your workflow will use for testing), add the necessary tasks to the campaign and inject the runs following [the instructions above](#launching-data-processings).
    - Remember to modify the template files to use the *dev-&lt;calibration name&gt;* docker image (*MY.SingularityImage* field), generate output and logs in a folder where the user has write access, and use your grid proxy path (*x509userproxy* field, should not be the ppsgit GRID proxy). All these variables should be substituted with the full path to their counterpart that should be used for testing.
4. Run the commands in the Jenkinsfile *automationApptainerStep* manually.
    - Remember to manually resolve the Jenkinsfile variables (e.g. dbinstance, campaign, eosdir, CMSSW_BASE, etc.).
5. You should see that, in the correct scenarios, jobs are submitted/re-submitted and task status update. Monitor this using [Grafana](https://ecal-automation-monit.web.cern.ch).
    - In order to transfer the log, output, and error files produced by the job (if they are being sent to /eos), use the `condor_transfer_data $(whoami)` command.

## Accessing run and job level information

All the information related to a given CMS run can be accessed (and modified) using the
[RunCtrl](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.RunCtrl)
class.

The `RunCtrl` class `init` function accepts two arguments that allow one to access information
from different processing instances. The `dbname` selects from with influxdb database the
information should be read, while the `campaign` parameter allows switching between different
processing campaigns stored in the same database. The snippet below illustrates how to
create an instance of the `RunCtrl`.

!!! note
    All the following examples uses the database configuration to access information
    for the ECAL prompt processing during 2022.

```py
from ecalautoctrl import RunCtrl

rctrl = RunCtrl(dbname='ecal_prompt_v1', campaign='prompt')
```

Likewise information related to single tasks (group of jobs executing one workflow for a
single run or groups of runs) and the single jobs within a task can be retrieved using
the [JobCtrl](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecal-jobctrl)
interface.

```py
from ecalautoctrl import JobCtrl

jctrl = JobCtrl(workflow='pulse-shapes-merge', campaign='prompt', tags={'run_number' : '359342', 'fill' : '8181'}, dbname="ecal_prompt_v1")

jctrl.taskCompleted()
jctrl.getFailed()
jctrl.getJob(jid=0, last=True)
```

!!! note
    The code snippet above provides a short example on how `JobCtrl` can be used to
    retrieve useful information. Please refer to the [ecalautoctrl docs][ecalautoctrl-docs]
    for more details.

##  Installation of ecalautoctrl with pip

The automation docker image comes with the [ecalautoctrl][ecalautoctrl-git] main version installed (see the [installation script](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docker/build.sh)).

The [ecalautoctrl python package][ecalautoctrl-git] can also be locally installed directly from the gitlab
repository using `pip install` for local usage:

```bash
python -m pip install git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/
```

This command works in any python environment (using `python -m` ensures that the package is
installed in the currently active environment). When installing the package within a CMSSW
release make sure to use the `python3 -m` since the `python` command is not automatically
mapped to `python3`.

For a clean and standalone installation using a dedicated [conda](https://docs.conda.io/projects/conda/en/latest/index.html) environment is strongly encouraged. This can be achieved with the following steps:

```bash
conda create -n py39-ectrl python==3.9
conda activate py39-ectrl
python -m pip install git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/
```

!!! note
    all the examples using the ecalautoctrl package can be run in any python interactive session or script.
    Consider installing `ipython` in the conda environment to have a simple yet powerful
    interactive session in which to execute them.


### Testing a different automation-control library

In order to develop new functionalities for the ecalautoctrl package, a local installation that can be used by the automation docker image can be necessary.

First of all, [connect to the docker image](#using-the-automation-image). The commands below, allow you to create a test environment with the [PPS automation repository][pps-automation-git] and a local ecalautoctrl package version:

```bash
cd path-to-test-dir

# Clone the automation repository (you might want your specific branch)
git clone https://gitlab.cern.ch/pps-dpg-tools/automation.git

# Clone the automation-control repository (either yours or ECAL's, depending on the need)
git clone https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git

# Add the automation control to the PYTHONPATH and PATH (PYTHON3PATH might be needed too)
export PATH=`pwd`/automation-control-pps/bin:`pwd`/automation-control-pps:$PATH
export PYTHONPATH=`pwd`/automation-control-pps/bin:`pwd`/automation-control-pps:$PYTHONPATH

# Go to your workflow directory and run your commands as if you were Jenkins
```

Whenever you log in the image again, you will need to do:

```bash
# Go to your test directory and setup the environment
cd path-to-test-dir
source /home/ppsgit/setup.sh

# Credentials for DB actions
export ECAL_INFLUXDB_USER=user
export ECAL_INFLUXDB_PWD=password

# Remember always to export the local automation-control repository
export PATH=`pwd`/automation-control-pps/bin:`pwd`/automation-control-pps:$PATH
export PYTHONPATH=`pwd`/automation-control-pps/bin:`pwd`/automation-control-pps:$PYTHONPATH

# Go to your workflow directory and run your commands as if you were Jenkins
```

## Collecting output files from a specific workflow

The output of each workflow can be retrieved using the [getOutput](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.RunCtrl.getOutput) method from `RunCtrl`.
The funtion take one mandatory argument: `process` that specify the output of which
workflow step is requested. One between the `runs`, `fills` and `era` arguments can also
be specified to narrow down the list of outputs to specific runs.

The example below show how to access the ECALElf ntuples produced on top of the WSkim for the
entire `Run2022C` acquisition era:
```py
from ecalautoctrl import RunCtrl

rctrl = RunCtrl(dbname='ecal_prompt_v1', campaign='prompt')
rctrl.getOutput(era='Run2022C', process='ecalelf-ntuples-wskim')
```

The above command will yield a list of output files (technically a python set to avoid duplicates) which should look similar to this:

<details><summary>Click to expand</summary>
<p>

```
[...]
 '/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357478/eleIDTree_0.root,/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357478/extraCalibTree_0.root,/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357478/extraStudyTree_0.root,/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357478/ntuple_0.root',
 '/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357479/eleIDTree_7.root,/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357479/extraCalibTree_7.root,/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357479/extraStudyTree_7.root,/eos/cms/store/group/dpg_ecal/alca_ecalcalib/automation_prompt/ecalelf/wskim/357479/ntuple_7.root'
[...]
```

</p>
</details>

!!! warning
    The automation package provides a single `output` field that is bounded to be a string
    in influxdb. Therefore, each workflow output field might store information in different
    ways. The `ecalelf-ntuples-wskim` workflow produces four output ROOT files for each
    job that are stored in the `output` field as a comma separated list.

## Manual interaction with the DB

Some commands for manually interacting with the DB are reported below. These are expert-level actions that should be performed only on test DBs. They can be useful to clean up a messed-up DB etc...

In order to perform the actions below:
1. [Connect to the docker unpacked image](#using-the-automation-image)
2. Export the DB credentials (ask an admin!) and setup the environment:<br>
```bash
export ECAL_INFLUXDB_USER=user
export ECAL_INFLUXDB_PWD=password
source /home/ppsgit/setup.sh
```
3. Start an interactive Python shell: `python3 -i`


### Manually delete some runs from a campaign

In the interactive Python shell do the following:<br>
```python
import influxdb
import os

# Connect to the DB called 'pps_test_v1'
client = influxdb.InfluxDBClient(host='ecal-automation-relay.web.cern.ch', port=80, username=os.getenv('ECAL_INFLUXDB_USER'), password=os.getenv('ECAL_INFLUXDB_PWD'), ssl=False, database='pps_test_v1')

# Select all points from the desired campaign
points = client.query('SELECT * FROM "run" WHERE campaign=\'test_tracking\' ').get_points()

# Find all runs to delete, e.g. all runs with number < 350000
runs_to_delete=[]
for point in points:
	if int(point['run_number']) < 350000:
		runs_to_delete.append(int(point['run_number']))

# Test to check that you are selecting really what you want to delete
for run in runs_to_delete:
	condition = f"run_number = \'{run}\'"
	query = f'SELECT * FROM "run" WHERE campaign=\'test_tracking\' AND {condition}'
	print(query)
	print(client.query(query))

# Delete permanently
for run in runs_to_delete:
	condition = f"run_number = \'{run}\'"
	query = f'DELETE FROM "run" WHERE campaign=\'test_tracking\' AND {condition}'
	print(query)
	client.query(query)

```

## Other useful commands

The `RunCtrl` class methods can be used/combined to retrieve useful information.
Same examples below.

Get an `era` start and end run using [getRunsInEra](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.RunCtrl.getRunsInEra):
```py
# agnostic
start, end = (min(rctrl.getRunsInEra(era='Run2022C')), max(rctrl.getRunsInEra(era='Run2022C')))

# requiring that the run was injected in a given workflow (for instance to avoid accounting
for Cosmics runs)
start, end = (min(rctrl.getRunsInEra(era='Run2022C', task='pulse-shapes')), max(rctrl.getRunsInEra(era='Run2022C', task='pulse-shapes')))
```

Likewise, use [getRunsInFill](https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/#ecalautoctrl.RunCtrl.getRunsInFill) to get all the runs belonging to a fill:
```py
# agnostic
rctrl.getRunsInFill(fill=8081)

# requiring that the run was injected in a given workflow (for instance to avoid accounting
for Cosmics runs)
rctrl.getRunsInFill(fill=8081, task='ecalelf-ntuples-wskim')
```
