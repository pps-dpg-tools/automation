# EOS monitor

This workflow monitors an EOS directory, storing in InfluxDB information about the quota usage. To limit the amount of data to be handled, the script executes at maximum one recursion (i.e. it digs into one sub-folder level).

A Grafana dashboard can be configured to display the results. For instance, for the PRO POG area, see [**here**](https://ecal-automation-monit.web.cern.ch/d/1LbZ28HIz/pro-pog-eos-monitor?orgId=1).

This tool is mainly made of one script, for which proper code documentation is provided [**here**](build/html/index.html#monitor-py-script).

## Updating quotas

In case of a change in the quotas or a new setup for a different EOS area, the config measurement in InfluxDB must be changed to have the Grafana tables reflect the correct quotas. 
To do so, use the auxiliary script documented [**here**](build/html/index.html#write-config-py-script).

## Instructions for manual execution

Similarly to what is done in the Jenkinsfile, to run the cleaner manually the user needs to:

- [Connect to the docker image](#using-the-automation-image)
- Remember to export the DB credentials (ask an admin!):<br>
```bash
export ECAL_INFLUXDB_USER=user
export ECAL_INFLUXDB_PWD=password
source /home/ppsgit/setup.sh
```
- Authenticate via kerberos as a user with write access to the files that will be deleted:<br>
```bash
kinit NICE_user
```
- Run the monitor: 
```bash
cd workflows/eos-monitor
# e.g. python3 monitor.py
```

For obvious reasons, the cleaner cannot be run locally on a user machine.