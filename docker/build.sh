#!/bin/bash

CMSSW_REL=$1

# exit when any command fails; be verbose
set -ex

# eos-client ("eos ls" and other stuff, not the actual deamon to mount the filesystem, that's done with the -B option by apptainer)
cat << EOF >>/etc/yum.repos.d/eos7-stable.repo
[eos8el-stable]
name=EOS binaries from CERN Linuxsoft [stable]
gpgcheck=0
enabled=1
baseurl=https://linuxsoft.cern.ch/internal/repos/eos8el-stable/x86_64/os/
priority=9
EOF
dnf install -y --disablerepo="epel-next" --nogpgcheck eos-client

# openssl
dnf install -y --disablerepo="epel-next" --nogpgcheck openssl-libs openssl-devel bc

# enable accessing T0 queues
dnf install -y --disablerepo="epel-next" --nogpgcheck environment-modules
mkdir -p /etc/modulefiles/lxbatch/
cat << EOF >> /etc/modulefiles/lxbatch/tzero
#%Module 1.0
# module file for lxbatch/share
#
setenv _myschedd_POOL tzero
setenv _condor_CONDOR_HOST "norwegianblue02.cern.ch, tzcm1.cern.ch"
EOF

# make cmsrel etc. work
shopt -s expand_aliases
source /cvmfs/cms.cern.ch/cmsset_default.sh

git config --global user.name '${SERVICE_USERNAME}'
git config --global user.email '${SERVICE_USERNAME}@cern.ch'
git config --global user.github '${SERVICE_USERNAME}'

mkdir -p /home/${SERVICE_USERNAME}/
cd /home/${SERVICE_USERNAME}/
cmsrel $CMSSW_REL
cd $CMSSW_REL/src
cmsenv
git cms-init
cd -

# manually install the required packages needed for ecalautoctrl
export PYTHON3PATH=$PYTHON3PATH:/home/${SERVICE_USERNAME}/lib/python3.9/site-packages/
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cmsoms/oms-api-client.git
cd oms-api-client/
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I .
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I influxdb
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I dbs3-client==3.17.6

# install ecalautoctrl
python3 -m pip install git+https://gitlab.cern.ch/tostafin/automation-control.git@dev-automation --prefix /home/${SERVICE_USERNAME}/ --no-deps -I

# copy lib and bin to the CMSSW area
mv /home/${SERVICE_USERNAME}/bin/* /home/${SERVICE_USERNAME}/$CMSSW_REL/bin/$SCRAM_ARCH/
mv /home/${SERVICE_USERNAME}/lib/python3.9/site-packages/* /home/${SERVICE_USERNAME}/$CMSSW_REL/python/

# Move back to base CMSSW directory
cd $CMSSW_BASE/src

# Add the PPS workflows
git cms-merge-topic CTPPS:rolling-calib-tracking-efficiency-14_1_4 # tracking-efficiency
git cms-merge-topic tostafin:rolling-calib-timing-resolution-14_1_4 # timing resolution
git cms-merge-topic tostafin:rolling-calib-timing-calibration-14_1_4 # timing calibration

# Compile
cmsenv
scram b -j

# set w/r permissions on home directory
chmod 777 /home/${SERVICE_USERNAME}/

# setup script
cat << EOF >> /home/${SERVICE_USERNAME}/setup.sh
#!/bin/bash

# Export variables necessary for DBS
export X509_USER_CERT=\$HOME/.globus/usercert.pem
export X509_USER_KEY=\$HOME/.globus/rsauserkey.pem
export X509_USER_PROXY=\$HOME/grid_proxy.x509

source /usr/share/Modules/init/sh
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/${SERVICE_USERNAME}/$CMSSW_REL
cmsenv
cd -
export PYTHONPATH=$PYTHON3PATH
EOF
