#!/bin/bash

set -ex

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Error occurred during script execution"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM ERR

source /home/ppsgit/setup.sh

export HOME=/afs/cern.ch/user/p/ppsgit/

# This is needed for omsapi to work
# False ^
# kinit ppsgit -k -t ppsgit.keytab

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh

set -E

cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Get the run number from the script arguments
run_number=$(echo "$TASK" | grep -oP "(?<=run_number:)\d+")

# Get the DB and campaign names
DB=$(echo "$TASK" | grep -oP "(?<=--db )[\w-_]+")
CAMPAIGN=$(echo "$TASK" | grep -oP "(?<=-c )[\w-_]+")

# Get the current iteration number
# TODO: we could inject this value based on the timing resolution iteration task Python file
ITERATION_TASK_DB_FIELD_NAME=timing-resolution-iteration-value
ITERATION=$(ecalrunctrl.py --db $DB status --campaign $CAMPAIGN --runs $run_number --noformat | grep -oP "(?<=$ITERATION_TASK_DB_FIELD_NAME=)\d+")

# Set the previous iteration number
LAST_ITERATION=$((ITERATION-1))

# Timing calibration workflow campaign name for the sqlite file
CALIB_CAMPAIGN_NAME=2024_double_peak_correction_v10

# # Make the following command independent of the json file used
mv *.json jsonFilter.json

# Run the job
if [ $ITERATION == 0 ]
then
    cmsRun $WDIR/src/Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py inputFiles=$INFILE globalTag=$GT planesConfig=$WDIR/src/Analyzer/DiamondTimingAnalyzer/planes2024.json sqlFileName=sqlite_file:/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/dev/pps-test-timing-calibration/$CALIB_CAMPAIGN_NAME/${run_number}/ppsDiamondTiming_calibration_${run_number}.sqlite jsonFileName=jsonFilter.json
else                                                                                                                                                                                    # calibInput from the harvester (last iteration)
    cmsRun $WDIR/src/Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py inputFiles=$INFILE globalTag=$GT planesConfig=$WDIR/src/Analyzer/DiamondTimingAnalyzer/planes2024.json calibInput=$EOSDIR/calibOutputTimingResolutionHarvester_run${run_number}_iteration${LAST_ITERATION}.json jsonFileName=jsonFilter.json
fi

mkdir -p $EOSDIR
OFILE=outputTimingResolutionWorker_${JOBID}.root
mv run_output.root $EOSDIR/$OFILE

ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE}"
