import sys
from ecalautoctrl import HTCHandlerByRun
        
if __name__ == '__main__':
    handler = HTCHandlerByRun(task='re-tracking-efficiency-analysis-harvester',
                              prev_input='re-tracking-efficiency-analysis-worker-with-reco',
                              deps_tasks=['re-tracking-efficiency-analysis-worker-with-reco'])

    ret = handler()
    
    sys.exit(ret)
