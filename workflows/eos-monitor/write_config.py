import subprocess
import os
import argparse
import copy
import ast
from tabulate import tabulate
from datetime import datetime
from influxdb import InfluxDBClient
from pprint import pprint
from ecalautoctrl.credentials import dbhost, dbusr, dbpwd, dbssl, dbport

def parse_dict_string(dict_string):
    try:
        parsed_dict = ast.literal_eval(dict_string)
        if not isinstance(parsed_dict, dict):
            raise ValueError("Invalid dictionary format")
        for key, value in parsed_dict.items():
            if not isinstance(key, str) or not (isinstance(value, float) or isinstance(value, int)):
                raise ValueError("Invalid key or value type in the dictionary. Values must be either float or int.")
        return parsed_dict
    except (ValueError, SyntaxError):
        raise argparse.ArgumentTypeError("Invalid dictionary format")

def main(args):
    db = InfluxDBClient(
        host=dbhost,
        port=dbport,
        username=dbusr,
        password=dbpwd,
        ssl=dbssl,
        database=args.db,
        timeout=30_000
    )

    # Point template
    time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
    point_template = {
        'measurement': args.measurement,
        'tags': {
            'group': 'test',
        },
        'time': time,
        'fields': {
            'quota': 0,
        }
    }

    points_to_write = []
    for group, quota in args.quotas.items():
        point = copy.deepcopy(point_template)
        point['tags']['group'] = group
        point['fields']['quota'] = quota
        points_to_write.append(point)
    
    # Delete previous config
    if not args.debug:
        print('[INFO] Deleting previous config')
        db.query(f'DELETE FROM {args.measurement}')

    print('[INFO] Config to be written to DB')
    print(tabulate(points_to_write, headers='keys', tablefmt='grid'))
    
    if args.debug:
        print('[DEBUG] Debug mode, not writing to DB')
    else:
        db.write_points(points_to_write)

def makeParser():
    # Default values
    DEBUG = False
    """Enable debug mode. Defaults to False."""
    DBNAME = 'pps_test_v1'
    """Name of the InfluxDB database to be used."""
    MEASUREMENT = 'eos_phys_pps_config'
    """Name of the InfluxDB measurement to be used."""
    QUOTAS = {
        'zh' : 6e+13,
        'zj' : 1e+13
    }
    """Dictionary of {group: quota} to be written to the database"""

    # Create the argument parser
    parser = argparse.ArgumentParser(description='Auxiliary script to write the EOS monitor config (quotas per user group) to the database', 
                                     formatter_class=argparse.RawTextHelpFormatter)
        
    parser.add_argument('--debug', action='store_true', default=DEBUG, help='Enable debug mode')
    parser.add_argument('--db', default=DBNAME, help='Name of the InfluxDB database to be used')
    parser.add_argument('-m','--measurement', default=MEASUREMENT, help='Name of the InfluxDB measurement to be used')
    parser.add_argument('--quotas', type=parse_dict_string, default=QUOTAS, 
        help="""Dictionary of {group: quota} to be written to the database. 
        Quotas must be written in Byte (SI) units. 
        Example: \"{\'zh\': 6e+13, \'zj\': 1e+13}\""""
    )
    return parser

if __name__ == "__main__":
    parser = makeParser()
    args = parser.parse_args()
    if args.debug:
        print("[DEBUG] Parsed dictionary:")
        pprint(args.quotas)
    main(args)
