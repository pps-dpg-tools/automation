#!/bin/bash

set -ex

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Error occurred during script execution"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM ERR

export HOME=/afs/cern.ch/user/p/ppsgit/

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh

set -E

cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Get the DB and campaign names
DB=$(echo "$TASK" | grep -oP "(?<=--db )[\w-_]+")
CAMPAIGN=$(echo "$TASK" | grep -oP "(?<=-c )[\w-_]+")

# Get the run number from the script arguments
RUN_NUMBER=$(echo "$TASK" | grep -oP "(?<=run_number:)\d+")

# Check if double peak correction is needed
DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME=timing-calibration-is-double-peak-correction-needed
IS_DOUBLE_PEAK_CORRECTION_NEEDED=$(ecalrunctrl.py --db $DB status --campaign $CAMPAIGN --runs $RUN_NUMBER --noformat | grep -oP "(?<=$DOUBLE_PEAK_CORRECTION_DB_FIELD_NAME=)\w+")

# Make the following command independent of the json file used
mv *.json jsonFilter.json

# Run the job
if [ "$IS_DOUBLE_PEAK_CORRECTION_NEEDED" == "True" ]
then
    cmsRun $WDIR/src/CalibPPS/TimingCalibration/test/DiamondCalibrationWorker_cfg.py inputFiles=$INFILE globalTag=$GT tVsLsFilename=$EOSDIR/ppsDiamondTiming_calibration_$RUN_NUMBER.root jsonFileName=jsonFilter.json
else
    cmsRun $WDIR/src/CalibPPS/TimingCalibration/test/DiamondCalibrationWorker_cfg.py inputFiles=$INFILE globalTag=$GT jsonFileName=jsonFilter.json
fi

mkdir -p $EOSDIR
OFILE=worker_output_$JOBID.root
if [ "$IS_DOUBLE_PEAK_CORRECTION_NEEDED" == "True" ]
then
    mv $EOSDIR/$OFILE $EOSDIR/worker_output_without_dp_correction_$JOBID.root
fi
mv worker_output.root $EOSDIR/$OFILE

ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE}"
