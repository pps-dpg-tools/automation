#!/bin/bash

set -ex

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Error occurred during script execution"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM ERR

export HOME=/afs/cern.ch/user/p/ppsgit/

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh

set -E

cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Get the run number from the script arguments
run_number=$(echo "$TASK" | grep -oP "(?<=run_number:)\d+")

# Run the job
cls=$(echo "$CLUSTERID" | sed 's/\./_/')
cmsRun $WDIR/src/RecoPPS/RPixEfficiencyTools/python/ReferenceAnalysisDQMHarvester_cfg.py inputFiles=$INFILE globalTag=$GT run=$run_number clusterid=$cls

mkdir -p $EOSDIR
OFILE=outputTrackingEfficiency_run$run_number.root
mv DQM*$run_number__$cls.root $EOSDIR/$OFILE

ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE}"
