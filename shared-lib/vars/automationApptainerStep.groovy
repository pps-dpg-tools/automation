#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: 'ppsgit_credentials', usernameVariable: 'PPSUSER', passwordVariable: 'PPSPWD')]) {
        // Wrap the script into a singularity exec
        s_script = "export KRB5CCNAME=/tmp/krb5cc_\$UID; echo '$PPSPWD' | kinit ppsgit; apptainer exec -B /cvmfs -B /eos -B /afs -B /etc/sysconfig/ngbauth-submit $image /bin/bash -c 'echo '$PPSPWD' | kinit ppsgit; $setup; $script'"
        sh(script: s_script)
    }
}