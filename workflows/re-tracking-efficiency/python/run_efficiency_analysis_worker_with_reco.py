import sys
import re
import logging
from ecalautoctrl import process_by_run, HTCHandler
from ecalautoctrl.CMSTools import QueryDBS
from dbsClient.apis.dbsClient import DbsApi
from typing import List, Dict, Optional, Union
        
def dbs_last_data_source(cls):
    """
    This decorator adds the :func:`get_files` that fetches data files from the 
    last version of the matching datasets on DBS.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.
    """

    def extract_sd_version_number(sd: str) -> Optional[str]:
        """Extract the version number from the secondary dataset string"""
        # Define the regular expression pattern
        pattern = re.compile(r'-v(\d+)$')
        # Use the search function to check for a match
        match = re.search(pattern, sd)
        # If a match is found, return the matched numbers; otherwise, return None
        return int(match.group(1)) if match else None

    def find_latest_dset(dsets: Dict) -> str:
        """Find the latest version of the dataset"""
        latest_dset = ''
        latest_version = 0
        for dset in dsets:
            version = extract_sd_version_number(dset['dataset'].split('/')[2])
            if version and version > latest_version:
                latest_dset = dset['dataset']
                latest_version = version
        return latest_dset

    def get_files_dbs_latest(self, runs: List[Dict]) -> List[str]:
        """
        Retrieve file names for given runs from the latest version of the DBS dataset.
        This function is added by :deco:`~ecalautoctrl.dbs_latest_data_source`.

        :param runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
        """
        flist = []
        try:
            if self.dsetname:
                dsets = [self.dsetname] if isinstance(self.dsetname, str) else self.dsetname
                for dset in dsets:
                    for run in runs:
                        # add era to dataset name, validating it in the process
                        _, pd, sd, tier = dset.split('/')
                        if run["era"] not in sd:
                            sd = '*' + run["era"] + sd
                        else:
                            sd = '*' + sd
                        fullname = ''
                        if extract_sd_version_number(sd):
                            # If the sd already contains a version number just search for the files
                            fullname = f'/{pd}/{sd}/{tier}'
                        else:
                            dbs = DbsApi('https://cmsweb.cern.ch/dbs/prod/global/DBSReader')
                            fullname_tmp = f'/{pd}/{sd}/{tier}'
                            dsets = dbs.listDatasets(dataset = fullname_tmp, run_num = int(run['run_number']))
                            if dsets:
                                latest_dset = find_latest_dset(dsets)
                                self.log.info('Found latest dataset for run '+run['run_number']+': ' + latest_dset)
                                fullname = latest_dset
                            else:
                                return flist
                        fromt0 = self.opts.t0 if 't0' in self.opts else False
                        dataQuery = QueryDBS(dataset=fullname)
                        flist.extend(dataQuery.getRunFiles(run=int(run['run_number']), fromt0=fromt0))
            return flist
        except AttributeError:
            raise AttributeError('Please define self.dsetname in your class when using the dbs_data_source decorator')
    
    setattr(cls, 'get_files', get_files_dbs_latest)
    return cls

@dbs_last_data_source
@process_by_run
class HTCHandlerByRunDBSLatest(HTCHandler):
    """
    Handler for the HTCondor jobs that process data by run, using the latest version of the DBS dataset.
    """
    def __init__(self,
                 task: str,
                 dsetname: Union[str, List[str]],
                 deps_tasks: Optional[List[str]] = None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname

if __name__ == '__main__':
    handler = HTCHandlerByRunDBSLatest(task='re-tracking-efficiency-analysis-worker-with-reco',
                              dsetname='/AlCaPPSPrompt/*/ALCARECO')

    ret = handler()    
    sys.exit(ret)
