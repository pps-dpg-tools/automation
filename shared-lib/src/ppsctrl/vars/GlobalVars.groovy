#!/usr/bin/env groovy
package ppsctrl.vars

class GlobalVars{
    static def setenv(script, jobname)
    {
        // Edit this with your subsystem variables
        switch(jobname) {
            case ~/.*-prompt$/:
                script.env.image = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/unpacked-images/automation:prompt" // Your unpacked docker image
                // script.env.image_setup = "source /home/ppsgit/setup.sh; module load lxbatch/tzero" // Your image setup script - will enable t0 queues
                script.env.image_setup = "source /home/ppsgit/setup.sh" // Your image setup script
                script.env.dbinstance = "pps_prompt_v1" // The InfluxDB where you write - usually different between prompt and repro
                script.env.campaign = "2024_prompt_v1" // The campaign tag, can also be a list of campaigns with a space in between
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3nhdbb94gibozrnmbofit9u7ih" // The mattermost hook for notification - optional functionality
                script.env.eospath = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/prompt" // Where you want to save stuff etc...
                script.env.eosplots = "/eos/your-plots" // Where you want to save plots etc... - optional functionality
                script.env.plotsurl = "https://your-plots.web.cern.ch/" // URL were the plots are plublished - optional functionality
                break
            case ~/.*-repro$/:
                script.env.image = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/unpacked-images/automation:repro"
                script.env.image_setup = "source /home/ppsgit/setup.sh"
                script.env.dbinstance = "pps_workflows_repro_2023"
                script.env.campaign = "all"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3nhdbb94gibozrnmbofit9u7ih"
                script.env.eospath = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/repro"
                script.env.eosplots = "/eos/your-plots"
                script.env.plotsurl = "https://your-plots.web.cern.ch/"
                break
            // Variables for special pipelines
            case ~/pps-grid-proxy-renewal/:
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3nhdbb94gibozrnmbofit9u7ih"
                break
            case ~/pps-htcondor-output-transfer/:
                script.env.image = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/unpacked-images/automation:dev"
                script.env.image_setup = "source /home/ppsgit/setup.sh"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3nhdbb94gibozrnmbofit9u7ih"
                break
            case ~/pps-unpacking/:
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3nhdbb94gibozrnmbofit9u7ih"
                script.env.imagepath = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/unpacked-images/"
            default:
                script.env.image = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/unpacked-images/automation:dev"
                script.env.image_setup = "source /home/ppsgit/setup.sh"
                script.env.dbinstance = "pps_test_v1"
                script.env.campaign = "test_tracking"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/zdw1wk58y7f45x11ih5ypymt5w"
                script.env.eospath = "/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/dev"
                script.env.eosplots = "/eos/your-plots"
                script.env.plotsurl = "https://your-plots-dev.web.cern.ch/"
                break
        }
    }
}
