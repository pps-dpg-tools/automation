# Tutorial: Running PPS Timing Calibration Workflow Manually

## What this tutorial is about?
It shows how to run the PPS calibration manually, without using the automation framework. 


## Prerequisites

- Access to the lxplus service.
- Access to the EOS filesystem (read permissions on the `/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/` directory).

## Steps to Run Timing Calibration

#### 1. Setup CMSSW Environment and the workspace on lxplus:
   Before proceeding, connect to the lxplus machine via ssh.
   ```bash
   cd /eos/user/<first-letter-of-your-name>/<your-cern-sso-login>
   mkdir manual-timing-calibration
   cd manual-timing-calibration/
   ```

#### 2. Setup environment and checkout the repo:
Check the CMSSW release version under the link [cmssw-release-version](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docker/cmssw_release.sh?ref_type=heads). In this tutorial, we use CMSSW_14_0_2, the latest release available at the time of writing. You can replace it with your version from the link above. We will use the branch `rolling-calib-timing-calibration-14_0_2` (change the branch name if you are using different release). The CTPPS/CMSSW repo is a PPS fork of the CMSSW repo created by the CMS (https://github.com/cms-sw/cmssw). 
   ```bash
   source /cvmfs/cms.cern.ch/cmsset_default.sh
   cmsrel CMSSW_14_0_2
   cd CMSSW_14_0_2/src/
   cmsenv # Setup the environment variables
   git cms-merge-topic CTPPS:rolling-calib-timing-calibration-14_0_2 
   ```

#### 3. Build the code:
   ```bash
   scram b -j # Scram is a C++ build tool used at CERN
   ```

#### 4. Run the worker:
   The calibration uses workers and harvesters. The worker configuration code is in the [/CalibPPS/TimingCalibration/test/DiamondCalibrationWorker_cfg.py](https://github.com/CTPPS/cmssw/blob/rolling-calib-timing-calibration-14_0_2/CalibPPS/TimingCalibration/test/DiamondCalibrationWorker_cfg.py) and the harvester config is in the [/CalibPPS/TimingCalibration/test/DiamondCalibrationHarvester_cfg.py](https://github.com/CTPPS/cmssw/blob/rolling-calib-timing-calibration-14_0_2/CalibPPS/TimingCalibration/test/DiamondCalibrationHarvester_cfg.py). 
   <br>The workers process the data in parallel. The results are then passed to the harvester, which produces the output of the calibration. 
   <br>First run the worker: 
   ```bash
   cmsRun ./CalibPPS/TimingCalibration/test/DiamondCalibrationWorker_cfg.py inputFiles=<input-file-path> globalTag=<global-tag> 
   ```

   The input files are placed on eos filesystem. The five files that we will use for the test are placed inside the `/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/` folder.
   
   If you want to look for another data to calibrate, you can explore the [CMS OMS](https://cmsoms.cern.ch/cms/runs/).

   The exemplary global tag that we will use is: ```140X_dataRun3_Prompt_v2``` 
   
   Let's run the worker and process the five input files. They are on EOS, so their paths needs to be preceded with `file:`.
   ```bash
   cmsRun ./CalibPPS/TimingCalibration/test/DiamondCalibrationWorker_cfg.py inputFiles=file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/05dce3b4-72b5-48e8-a276-fba8d5a77627.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/2d4b154a-4d6a-4d11-9001-a071212219bc.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/31bf65ff-a82b-4465-bd00-cbdab8f93164.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/61ed4218-4db5-4949-a67a-008d2b8bfd69.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/75551549-33e6-477c-aab7-04534f4d8477.root globalTag=140X_dataRun3_Prompt_v2
   ```

#### 5. Then, run the harvester on the file created by the worker:
   The `.root` file produced by the worker will be an input to the harvester. Its name should be `worker_output.root`.
   The command that runs the harvester is:
   ```bash
   cmsRun ./CalibPPS/TimingCalibration/test/DiamondCalibrationHarvester_cfg.py inputFiles=worker_output.root globalTag=140X_dataRun3_Prompt_v2 thresholdFractionOfMax=0.05
   ```
   The inputFiles parameter is a comma-separated list of paths to the workers output files (in our case it's just one file).
   The globalTag is the same as before and the thresholdFractionOfMax is 0.05 and was left as the default.

#### 6. The timing calibration results:
   The harvester produces two files in the directory, when the cmsRun command was executed. There is one `.root` file and one `.sqlite`, as shown on the picture below: ![alt text](./img/calibration-files.png)
