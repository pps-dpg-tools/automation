#!/bin/bash

set -e

CMSSW_PATH=~/scratch42
CMSSW_REL=CMSSW_14_0_2
ITERATION_NUMBER=9
TIMING_RESOLUTION_RESULTS_PATH=/eos/cms/store/group/dpg_ctpps/comm_ctpps/pps-automation/results/timing-resolution/2024/
RECORD=PPSTimingCalibrationRcd
TAG=DiamondTimingCalibration
MERGED_SQLITE_PATH=~


mkdir $CMSSW_PATH
cd $CMSSW_PATH
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsrel $CMSSW_REL
cd $CMSSW_REL/src
cmsenv
git cms-merge-topic CTPPS:json-sqlite-converter

readarray -t files < <(find $TIMING_RESOLUTION_RESULTS_PATH -mindepth 1 -maxdepth 1 -type d | sort)

convert_json_to_sqlite()
{
    run_number=$1
    json_calib_output_filename=calibOutputTimingResolutionHarvester_run${run_number}_iteration${ITERATION_NUMBER}.json
    json_calib_output_path=${files[i]}/${json_calib_output_filename}
    cmssw_json_calib_output_path=${CMSSW_BASE}/src/CalibPPS/ESProducers
    cp $json_calib_output_path $cmssw_json_calib_output_path

    json_sqlite_converter_path=${CMSSW_BASE}/src/CalibPPS/ESProducers/test/ppsTimingCalibrationWriter_cfg.py
    cmssw_json_calib_output_path=${cmssw_json_calib_output_path}/${json_calib_output_filename}
    cmsRun $json_sqlite_converter_path runNumber=$run_number cmsswJsonCalibOutputPath=$cmssw_json_calib_output_path record=$RECORD tag=$TAG

    rm $cmssw_json_calib_output_path
}

# Treat the first file in a special way because we want to use this SQLite as our destdb
first_run_number=$(echo "${files[0]}" | awk -F/ '{ print $NF }')
convert_json_to_sqlite $first_run_number
dest_db_filename=ppsDiamondTiming_calibration${first_run_number}.sqlite

for ((i=1; i < ${#files[@]}; ++i)); do
    curr_run_number=$(echo "${files[$i]}" | awk -F/ '{ print $NF }')
    convert_json_to_sqlite $curr_run_number
    conddb --yes --db ppsDiamondTiming_calibration${curr_run_number}.sqlite copy $TAG --destdb $dest_db_filename
    rm ppsDiamondTiming_calibration${curr_run_number}.sqlite
done

mv $dest_db_filename $MERGED_SQLITE_PATH
echo Merged SQLite path: ${MERGED_SQLITE_PATH}/${dest_db_filename}

rm -rf $CMSSW_PATH
