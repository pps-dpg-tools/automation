import subprocess
import os
import copy
import argparse
from tabulate import tabulate
from datetime import datetime
from influxdb import InfluxDBClient
from pprint import pprint
from ecalautoctrl.credentials import dbhost, dbusr, dbpwd, dbssl, dbport

def get_eos_ls_output(folder_path,mgm_url):
    """Run `eos ls -la` on the specified folder and return the output.

    :param folder_path: path to the folder
    :type folder_path: str
    :return: output of the `eos ls -la` command
    :rtype: str
    """    
    command = f"eos {mgm_url} ls -la {folder_path}"
    print(f'[DEBUG] Running command: {command}')
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    return result.stdout

def parse_ls_output(ls_output, current_level, parent_directory, top_directory):
    lines = ls_output.split('\n')
    file_info_list = []

    for line in lines[1:]:  # Skip the header line
        if line.strip():
            parts = line.split()
            name = parts[-1]
            if not name.startswith('.') and name not in ('.', '..'):
                if  parts[2].isdigit():
                    print(f"[WARNING] File '{os.path.relpath(os.path.join(parent_directory, name), top_directory)}' has an invalid owner. Ignoring.")
                    continue
                file_info = {
                    'name': os.path.relpath(os.path.join(parent_directory, name), top_directory),
                    'owner': parts[2],
                    'group': parts[3],
                    'size': int(parts[4]),
                    'is_directory': parts[0][0] == 'd',
                    'level': current_level,
                    'date': get_last_modification_date(os.path.join(parent_directory, name), parts[0][0] == 'd')
                }
                file_info_list.append(file_info)

    return file_info_list

def get_last_modification_date(path, is_directory):
    if is_directory:
        return get_last_modification_date_for_directory(path)
    else:
        return get_last_modification_date_for_file_or_link(path)

def get_last_modification_date_for_file_or_link(file_path):
    try:
        stat_info = os.lstat(file_path)
        timestamp = stat_info.st_mtime
        if timestamp < datetime.now().timestamp():
            return format_timestamp(timestamp)
        else:
            print(f"[WARNING] File '{file_path}' has a modification date in the future. Ignoring.")
            return format_timestamp(0)
    except OSError:
        return 'N/A'

def get_last_modification_date_for_directory(directory_path):
    max_mtime = 0
    for root, dirs, files in os.walk(directory_path):
        for file in files:
            file_path = os.path.join(root, file)
            stat_info = os.lstat(file_path)
            mtime = stat_info.st_mtime
            if mtime < datetime.now().timestamp():
                max_mtime = max(max_mtime, stat_info.st_mtime)
        for directory in dirs:
            dir_path = os.path.join(root, directory)
            stat_info = os.lstat(dir_path)
            mtime = stat_info.st_mtime
            if mtime < datetime.now().timestamp():
                max_mtime = max(max_mtime, stat_info.st_mtime)
    return format_timestamp(max_mtime)

def format_timestamp(timestamp):
    date = datetime.fromtimestamp(timestamp)
    return date.strftime('%Y-%m-%dT%H:%M:%SZ')

def display_table(file_info_list):
    headers = ['Name', 'Owner', 'Group', 'Size', 'Is Directory', 'Level', 'Last Modification Date']
    table_data = [(info['name'], info['owner'], info['group'], info['size'], info['is_directory'], info['level'], info['date']) for info in file_info_list]
    print(tabulate(table_data, headers=headers, tablefmt='grid'))

def process_directory(directory_path, args, current_level, all_file_info_list):
    if current_level > 1:
        return
    print(f"[INFO] Processing directory '{directory_path}'")

    ls_output = get_eos_ls_output(directory_path,args.eos_mgm_url)
    file_info_list = parse_ls_output(ls_output, current_level, directory_path, args.directory)
    all_file_info_list.extend(file_info_list)

    subdirectories = [f['name'] for f in file_info_list if f['is_directory']]
    for subdir in subdirectories:
        subdir_path = os.path.join(directory_path, subdir)
        process_directory(subdir_path, args, current_level + 1, all_file_info_list)

def sync_with_db(dbname, measurement, file_info_list, debug):

    time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

    db = InfluxDBClient(
        host=dbhost,
        port=dbport,
        username=dbusr,
        password=dbpwd,
        ssl=dbssl,
        database=dbname,
        timeout=30_000
    )

    # Point template
    point_template = {
        'measurement': measurement,
        'tags': {
            'name': 'test',
            'owner': 'test',
            'group': 'test',
            'is_directory': False,
            'level': -1,
            'present': False 
        },
        'time': time,
        'fields': {
            'size': 0,
            'mtime': datetime.now()
        }
    }

    points_to_write = []
    entries_no_longer_present = 0
    current_entries = 0

    # Query the database for all the present files
    # Create a set to store the names of files present in 'file_info_list'
    present_files = set(entry['name'] for entry in file_info_list)
    query = f'SELECT * FROM {measurement} WHERE "present" = True'
    # Mark files/directories that are any longer present with 'present' = False
    for entry in db.query(query).get_points():
        if entry['name'] not in present_files:
            entry['present'] = False
            entries_no_longer_present += 1
            points_to_write.append(entry)

    # Add an entry for all the files that are present, if they are not already present
    for entry in file_info_list:
        point = copy.deepcopy(point_template)
        point['tags']['name'] = entry['name']
        point['tags']['owner'] = entry['owner']
        point['tags']['group'] = entry['group']
        point['tags']['is_directory'] = entry['is_directory']
        point['tags']['level'] = entry['level']
        point['tags']['present'] = True
        point['fields']['size'] = entry['size']
        point['fields']['mtime'] = entry['date']
        current_entries += 1
        points_to_write.append(point)
    
    print(f"[INFO] Entries no longer present: {entries_no_longer_present}")
    print(f"[INFO] Current entries: {current_entries}")
    if debug:
        print(f"[DEBUG] Points to write:")
        pprint(points_to_write)
        print(f"[DEBUG] Debug mode, not writing to DB")
    else:
        db.write_points(points_to_write)
        pass


def main(args):
    all_file_info_list = []

    if os.path.exists(args.directory):
        process_directory(args.directory, args, current_level=0, all_file_info_list=all_file_info_list)
        if args.debug:
            print(f"[DEBUG] All file info list:")
            display_table(all_file_info_list)
        sync_with_db(args.db, args.measurement, all_file_info_list, args.debug)
    else:
        print(f"[ERROR] Directory '{args.directory}' does not exist.")

def makeParser():
    # Default values
    DEBUG = False
    """Enable debug mode. Defaults to False."""
    EOS_MGM_URL = 'root://eoscms.cern.ch'
    """MGM for EOS commands. Defaults to the CMS EOS."""
    EOS_ROOT_DIR = '/eos/cms/store/group/phys_pps'
    """Root directory to be inspected. Defaults to the PPS POG group area."""
    DBNAME = 'pps_test_v1'
    """Name of the InfluxDB database to be used."""
    MEASUREMENT = 'eos_phys_pps'
    """Name of the InfluxDB measurement to be used."""

    # Use argparse to change the global variables
    parser = argparse.ArgumentParser(
        description='Monitor EOS directories and store the information in an InfluxDB database.'
    )
    parser.add_argument('--eos-mgm-url', default=EOS_MGM_URL, help='MGM for EOS commands')
    parser.add_argument('--db', default=DBNAME, help='Name of the InfluxDB database to be used')
    parser.add_argument('-d','--directory', default=EOS_ROOT_DIR, help='Root directory to be monitored')
    parser.add_argument('-m','--measurement', default=MEASUREMENT, help='Name of the InfluxDB measurement to be used')
    parser.add_argument('--debug', action='store_true', default=DEBUG, help='Enable debug mode')

    return parser

if __name__ == "__main__":
    parser = makeParser()
    args = parser.parse_args()

    if args.debug:
        print(f"[DEBUG] Running parameters:")
        print(f"[DEBUG] EOS_MGM_URL = {args.eos_mgm_url}")
        print(f"[DEBUG] EOS_ROOT_DIR = {args.directory}")
        print(f"[DEBUG] DBNAME = {args.db}")
        print(f"[DEBUG] MEASUREMENT = {args.measurement}")
    main(args)
