#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}
EFFICIENCYFILE=${8}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/p/ppsgit/

# Setup CMSSW and move back to the running dir
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

# Set the job as running in the DB
ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Run the job
cmsRun $WDIR/src/RecoPPS/RPixEfficiencyTools/python/ReferenceAnalysisDQMWorker_cfg.py inputFiles=$INFILE globalTag=$GT efficiencyFileName=$EFFICIENCYFILE
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    mkdir -p $EOSDIR
    OFILE=outputReferenceAnalysisDQMWorker_$JOBID.root
    mv outputReferenceAnalysisDQMWorker.root $EOSDIR/$OFILE
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
