import subprocess
from to_remove import to_remove
from datetime import datetime,timedelta
from typing import List
from influxdb import InfluxDBClient

from ecalautoctrl.credentials import dbhost, dbport, dbusr, dbpwd, dbssl
from ecalautoctrl.influxdb_utils import reformat_last
from ecalautoctrl.JobCtrl import JobCtrl, JobStatus

DEBUG = False
"""Enable debug messages"""
DBNAME = 'pps_test_v1'
"""Name of the InfluxDB database to be inspected"""
DELETED_MESSAGE = 'deleted by cleaner'
"""Message to be written in the DB when a file is deleted"""
TIME_HORIZON = timedelta(days=60) 
""" Maximum look-back period"""

class Cleaner:
    def __init__(self, dbname : str = ''):
        self.db = InfluxDBClient(host=dbhost,
                        port=dbport,
                        username=dbusr,
                        password=dbpwd,
                        ssl=dbssl,
                        database=DBNAME,
                        timeout=30_000)

    def getFinishedRuns(self, campaign : str = '') -> List[str]:
        """Get the list of finished runs for the selected campaign.

        :param campaign: campaign for which the finished runs will be retrieved, defaults to ''
        :type campaign: str, optional
        :return: list of finished runs
        :rtype: List[str]
        """
        # Selection of the runs that are finished
        selection = '"last_global"=\'done\''

        # Starting time for the search
        start_time = datetime.utcnow() - TIME_HORIZON
        selection += f' AND time >= \'{start_time.strftime("%Y-%m-%dT%H:%M:%SZ")}\''

        query = f'''SELECT *,"run_number" FROM 
            (SELECT last(*) FROM "run" WHERE "campaign"=\'{campaign}\' GROUP BY run_number,fill)
            WHERE {selection} GROUP BY "run_number"'''
        results = reformat_last(self.db.query(query))

        output = []
        for result in results:
            if 'run_number' in result:
                output.append(result['run_number'])
        if DEBUG:
            print('Cleaner.getFinishedRuns: \nquery = {}'.format(query))
            print('Cleaner.getFinishedRuns: \nresults = {}'.format(results))
            print('Cleaner.getFinishedRuns: \noutput = {}'.format(output))
        
        return output

    def cleanFields(self, campaign : str = '', task : str = '', fields_to_delete : List[str] = []) -> None:
        """Delete the fields from the jobs linked to the task of the selected campaign, if the run is completed.
        Mark the deleted fields with :const:`DELETED_MESSAGE` in the DB. 
        The function has a maximum look-back period of :const:`TIME_HORIZON`.

        :param campaign: campaign for which deletion will happen, defaults to ''
        :type campaign: str, optional
        :param task: task for which deletion will happen, defaults to ''
        :type task: str, optional
        :param fields_to_delete: fields of the job measurement that will be deleted, defaults to []
        :type fields_to_delete: List[str], optional
        """
        runs_to_consider = self.getFinishedRuns(campaign)

        files_to_delete = []

        for run in runs_to_consider:
            jctrl = JobCtrl(dbname = DBNAME, task = task, campaign = campaign, tags = {'run_number': run})
            
            jobs = jctrl.getJobs()
            if DEBUG:
                print('Cleaner.cleanFields: \nrun = {}, task = {}, campaign = {}'.format(run,task,campaign))
                print('Cleaner.cleanFields: \njobs = {}'.format(jobs))

            for status in jobs:
                if status != 'done' and len(jobs[status])!=0:
                    print('[WARNING] Deleting files for campaign {}, run {}, task {}: job with status {}.'.format(campaign,run,task,status))
                for jid in jobs[status]:
                    job = jctrl.getJob(jid,last=True)[0]
                    if DEBUG:
                        print('Cleaner.cleanFields: \njob with id {} = {}'.format(jid,job))
                    
                    for field in fields_to_delete:
                        if field not in job:
                            print('[WARNING] Deleting files for campaign {}, run {}, task {}: job with id {} does not have field {}.'.format(campaign,run,task,jid,field))
                        else:
                            files_to_delete.append(job[field])
                            print('[INFO] Deleting files for campaign {}, run {}, task {} job with id {} field {}: {}'.format(campaign,run,task,jid,field,job[field]))

                            # Tell the DB that the file was deleted
                            if not DEBUG:
                                jctrl.setStatus(jid,status = JobStatus[status],fields = {field:DELETED_MESSAGE})
                            if DEBUG:
                                print('Cleaner.cleanFields: \nwould execute {}'.format(f'jctrl.setStatus(jid,status = {JobStatus[status]},fields = {{\'{field}\':\'{DELETED_MESSAGE}\'}})'))

        # Remove any possible duplicates
        files_to_delete = list(set(files_to_delete))
        if DELETED_MESSAGE in files_to_delete:
            files_to_delete.remove(DELETED_MESSAGE)

        if DEBUG:
            print('Cleaner.cleanFields: \nfiles_to_delete = {}'.format(files_to_delete))

        if len(files_to_delete) == 0:
            print('[INFO] No files to delete.')
            return
        # Delete the files
        if DEBUG:
            print('Cleaner.cleanFields: \nwould execute {}'.format(['rm']+files_to_delete))
        try:
            if not DEBUG:
                subprocess.run(['rm']+files_to_delete)
        except subprocess.CalledProcessError as e:
                print(f"Error while deleting: {e}")

if __name__ == '__main__':
    for campaign in to_remove:
        cleaner = Cleaner(campaign)
        for task in to_remove[campaign]:
            fields_to_delete = []
            for field in to_remove[campaign][task]:
                if DEBUG:
                    print("Removing {} of {} task in {}".format(field, task, campaign))
                fields_to_delete.append(field)
            
            # Delete the fields
            cleaner.cleanFields(campaign, task, fields_to_delete)
            