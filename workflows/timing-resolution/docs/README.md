# Timing resolution workflow docs

## Tutorial: Running PPS Timing Resolution Workflow Manually

The tutorial shows how to run the timing resolution workflow manually, without using the automation framework.


### Prerequisites

- Access to the lxplus service.
- Access to the EOS filesystem (read permissions on the `/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/` directory).

### Steps to Run Timing Resolution

#### 1. Setup CMSSW Environment and the workspace on lxplus:
   Before proceeding, connect to the lxplus machine via ssh.

   ```bash
   cd /eos/user/<first-letter-of-your-name>/<your-cern-sso-login>
   mkdir timing-resolution
   cd timing-resolution/
   ```

#### 2. Setup environment, checkout the repo and build the code:
Check the CMSSW release version under the link [cmssw-release-version](https://gitlab.cern.ch/pps-dpg-tools/automation/-/blob/master/docker/cmssw_release.sh?ref_type=heads). In this tutorial, we use CMSSW_14_0_2, the latest release available at the time of writing. You can replace it with your version from the link above. We will use the branch `rolling-calib-timing-resolution-14_0_2` (change the branch name if you are using different release).
   ```bash
   source /cvmfs/cms.cern.ch/cmsset_default.sh
   cmsrel CMSSW_14_0_2
   cd CMSSW_14_0_2/src/
   cmsenv # Setup the environment variables
   git cms-merge-topic CTPPS:rolling-calib-timing-resolution-14_0_2
   scram b -j # Scram is a C++ build tool used at CERN
   ```

#### 3. Run the worker for the first time:
   The calibration uses workers and harvesters. The worker configuration code is in the [Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py](https://github.com/CTPPS/cmssw/blob/rolling-calib-timing-resolution-14_0_2/Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py) and the harvester config is in the [Analyzer/DiamondTimingAnalyzer/python/harvester.py](https://github.com/CTPPS/cmssw/blob/rolling-calib-timing-resolution-14_0_2/Analyzer/DiamondTimingAnalyzer/python/harvester.py).
   <br>The files produced by the worker are then passed to the harvester, which produces the output of the calibration. The  process is then repeated until the results are sufficient.
   <br>The command to run the worker in the iteration zero is:
   ```bash
   cmsRun ./Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py inputFiles=<input-file-path> globalTag=<global-tag> planesConfig=<planes-config-json-file> sqlFileName=sqlite_file:<sqlite-file>
   ```
   It's worth noticing, that the `sqlFileName` parameter is only provided for the iteration zero. In the next iterations the `calibInput` should be provided instead.

   The `.root` and `.sqlite` input files are placed on eos filesystem inside the `/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/` folder.

   If you want to look for another data to calibrate, you can explore the [CMS OMS](https://cmsoms.cern.ch/cms/runs).

   The exemplary global tag that we will use is ```140X_dataRun3_Prompt_v2``` and the `planesConfig` is ` ./Analyzer/DiamondTimingAnalyzer/planes2024.json`.  The data come from `run 379661`.

   Let's run the worker and process the five input files. Other runs can have different number of root files than the one used for demo. Each of the five workers processes its own file and their job is paralellized in the automation framework. The files are stored on EOS, so their paths needs to be preceded with `file:`.
   ```bash
   cmsRun ./Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py inputFiles=file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/05dce3b4-72b5-48e8-a276-fba8d5a77627.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/2d4b154a-4d6a-4d11-9001-a071212219bc.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/31bf65ff-a82b-4465-bd00-cbdab8f93164.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/61ed4218-4db5-4949-a67a-008d2b8bfd69.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/75551549-33e6-477c-aab7-04534f4d8477.root globalTag=140X_dataRun3_Prompt_v2 planesConfig=./Analyzer/DiamondTimingAnalyzer/planes.json sqlFileName=sqlite_file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/ppsDiamondTiming_calibration.sqlite
```
Instead of running the worker with five input files (it will process them sequentially), you can run five workers and each of them will have one `.root` file to process. In that case, the workers can run in parallel.
   The output of the worker is the `run_output.root` file.<br>
   ![alt text](./img/worker-output-files.png)


#### 5. Then, run the harvester on the file created by the worker:
   The `.root` files produced by the worker will be an input to the harvester.
   The command that runs the harvester is:
   ```bash
   cmsRun ./Analyzer/DiamondTimingAnalyzer/python/harvester.py globalTag=140X_dataRun3_Prompt_v2  rootFiles=file:./run_output.root sqlFileName=sqlite_file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/ppsDiamondTiming_calibration.sqlite
   mv calib.json calib-0.json
   ```
   The `sqliteFileName` provided to the harvester has to be the same as for the worker.
   The zero in the `calib-0.json` stands for the iteration number.
   The inputFiles parameter is a comma-separated list of paths to the workers output files (in our case it's just one file, example on how to use it with more input files is in the section 6 below).
   The globalTag is the same as before. The harvester produces one .json file and one .root file, as shown below:
   ![alt text](./img/harvester-output-files.png)

#### 6. Run the worker and the harvester in iterations:
Then run the worker and harvester in iterations to improve the timing resolution. You can repeat this step until you obtain results that satisfy you.
The code for the worker:
```
cmsRun ./Analyzer/DiamondTimingAnalyzer/python/worker_withReRecoShift.py inputFiles=file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/05dce3b4-72b5-48e8-a276-fba8d5a77627.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/2d4b154a-4d6a-4d11-9001-a071212219bc.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/61ed4218-4db5-4949-a67a-008d2b8bfd69.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/31bf65ff-a82b-4465-bd00-cbdab8f93164.root,file:/eos/project-c/ctpps/subsystems/Automation/documentation/example-files/75551549-33e6-477c-aab7-04534f4d8477.root globalTag=140X_dataRun3_Prompt_v2 planesConfig=./Analyzer/DiamondTimingAnalyzer/planes2024.json calibInput=calib-<previous-iteration-number>.json
```

Then run the code for the harvester:
```
cmsRun ./Analyzer/DiamondTimingAnalyzer/python/harvester.py globalTag=140X_dataRun3_Prompt_v2 rootFiles=file:./run_output.root loopIndex=<current-interation-number> calibFiles=<list-of-all-previous-calib-json-files> calibInput=./calib-<previous-iteration-number>.json
mv calib.json calib-<current-iteration-number>.json
```
Iteration number is one, when you run the harvester for the second time, as it counts from zero.
For example, when we run the iteration three, the command will look as follows:
```
cmsRun ./Analyzer/DiamondTimingAnalyzer/python/harvester.py globalTag=140X_dataRun3_Prompt_v2 rootFiles=./run_output.root loopIndex=3 calibFiles=calib-0.json,./calib-1.json,./calib-2.json calibInput=./calib-2.json
mv calib.json calib-3.json
```

#### 7. The timing resolution results:
   The harvester produces two files in the directory, when the cmsRun command was executed. There is one `.root` file and `.json` calibration files.
   ![alt text](./img/harvester-output-files.png).


## Tutorial: Running JSON -> SQLite converter and merger
This tutorial shows how to run the script for converting the timing resolution JSON results into SQLite files and merge them into a single one.

### Prerequisites
- Access to the lxplus service.
- Script located at `workflows/timing-resolution/tools/json_sqlite_converter_and_merger.sh`

### Steps

#### 1. Substitute variables in the script
If necessary (defaults are provided), change the values of the following variables which appear at the beginning of the script:

- `CMSSW_PATH`: path where a CMSSW repo will be cloned.

- `CMSSW_REL`: version of the CMSSW repo.

- `ITERATION_NUMBER`: iteration number used for fetching the right JSON calibration output. **Note:** iterations start from 0, so for instance `ITERATION_NUMBER=9` means: get the results after 10 iterations.

- `TIMING_RESOLUTION_RESULTS_PATH`: path where the timing resolution results are stored. This path is expected to have directories named after a run number and inside a file named `calibOutputTimingResolutionHarvester_run${run_number}_iteration${ITERATION_NUMBER}.json` (the timing resolution workflow does that by default!).

- `RECORD`: record used for converting to the SQLite file.

- `TAG`: tag used for converting to the SQLite file.

- `MERGED_SQLITE_PATH`: path to store the merged SQLite file.

#### 2. Allow executing the script
Run `chmod +x <path-to-script>`.

#### 3. Execute the script
Run `bash <path-to-script>`. At the end of a sucessful run the script will print out the merged SQLite file path.
