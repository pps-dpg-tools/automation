import sys

from ecalautoctrl import HTCHandlerByRun


if __name__ == '__main__':
    handler = HTCHandlerByRun(task='timing-resolution-harvester',
                              prev_input='timing-resolution-worker',
                              deps_tasks=['timing-resolution-worker'])

    ret = handler()
    sys.exit(ret)
